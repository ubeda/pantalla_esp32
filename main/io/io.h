/*
 * io.h
 *
 *  Created on: 5 mar. 2021
 *      Author: ismael.casaban
 */

#ifndef MAIN_IO_H_
#define MAIN_IO_H_

#include <stdint.h>

#include "esp_event.h"
#include "esp_err.h"

extern time_t timeScreen;

esp_err_t board_io_init(void);
void board_io_loop(void);

void board_io_set_modem_power(uint8_t value);
void board_io_set_buzzer(uint8_t value);
void board_io_set_modem_pwr(uint8_t value);
void board_io_set_lcd_light(uint8_t value);

void board_io_set_led(uint8_t num, uint8_t value);
void board_io_set_leds(uint8_t value);

// Declare an event base
ESP_EVENT_DECLARE_BASE(EVENTS_KEYBOARD);        //!< declaration of the keyboard events family

typedef enum {                                  //!< declaration of the specific events under the keyboard event family
	EVENTS_KEYBOARD_KEY_1,
	EVENTS_KEYBOARD_KEY_2,
	EVENTS_KEYBOARD_KEY_3,
	EVENTS_KEYBOARD_KEY_4,
	EVENTS_KEYBOARD_KEY_UP,
	EVENTS_KEYBOARD_KEY_LEFT,
	EVENTS_KEYBOARD_KEY_OK,
	EVENTS_KEYBOARD_KEY_RIGHT,
	EVENTS_KEYBOARD_KEY_DOWN,
	EVENTS_KEYBOARD_NO_KEY_1,
	EVENTS_KEYBOARD_NO_KEY_2,
	EVENTS_KEYBOARD_NO_KEY_3,
	EVENTS_KEYBOARD_NO_KEY_4,
	EVENTS_KEYBOARD_NO_KEY_UP,
	EVENTS_KEYBOARD_NO_KEY_LEFT,
	EVENTS_KEYBOARD_NO_KEY_OK,
	EVENTS_KEYBOARD_NO_KEY_RIGHT,
	EVENTS_KEYBOARD_NO_KEY_DOWN,
	EVENTS_TIMER_REFRESH_DISPLAY
}events_keyboard_e;

#endif /* MAIN_IO_H_ */
