/*
 * io.c
 *
 *  Created on: 5 mar. 2021
 *      Author: ismael.casaban
 */

#include "nvs_flash.h"
#include "esp_event.h"

#include "esp_log.h"
#include "stdint.h"

//#include "hal_utils.h"
//#include "hal_io.h"
#include "string.h"

#include "mcp23x17.h"

#include "io.h"
//#include "conf.h"
#include "display.h"
//#include "board.h"
#include "def.h"
#include "config.h"

static const char *TAG = "IO";

#define GPIO_PIN_LED_0	10
#define GPIO_PIN_LED_1	8
#define GPIO_PIN_LED_2	6
#define GPIO_PIN_LED_3	5

#define GPIO_MODEM_PWR	0
#define GPIO_BUZZER		2
#define GPIO_LCD_LIGHT	3

#define GPIO_KEY_0		11
#define GPIO_KEY_1		9
#define GPIO_KEY_2		7
#define GPIO_KEY_3		4
#define GPIO_KEY_4		15
#define GPIO_KEY_5		1
#define GPIO_KEY_6		12
#define GPIO_KEY_7		14
#define GPIO_KEY_8		13

#define GPIO_KEY_MAX	9

#define ESP_INTR_FLAG_DEFAULT 0

#define EXPANSOR_GPIO_MODEM_PWR 0

/* Event source periodic timer related definitions */
ESP_EVENT_DEFINE_BASE(EVENTS_KEYBOARD);

mcp23x17_t mcp23x17;
static xQueueHandle gpio_evt_queue = NULL;

/* ISABEL */
extern Interfaz_t Interfaz;
/**********/

static void gpio_isr_handler(void* arg);
void __io_key_detect(uint16_t port_data);

uint16_t io_keyboard_last_status = 0xFFFF;
uint64_t io_keyboard_times[GPIO_KEY_MAX];

esp_err_t board_io_init(void){		//Inicializa el chip mcp23x17 con comunicaci�n I2C y que gestiona: los 4 LEDs, las 9 teclas y 3 pins de salida (pwr, buzzer y lcd light)

	memset(&mcp23x17, 0, sizeof(mcp23x17_t));

	i2cdev_init();

	//Initialize GPIO extension
	ESP_ERROR_CHECK(mcp23x17_init_desc(&mcp23x17, 0, MCP23X17_ADDR_BASE, SDA_GPIO, SCL_GPIO));  //Initialize device descriptor "mcp23x17" with I2C configuration. Device-port-address-sda-scl

	//For the corresponding pin, set either output mode or input mode

	//4 LEDs. Output mode
	mcp23x17_set_mode(&mcp23x17, GPIO_PIN_LED_0, MCP23X17_GPIO_OUTPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_PIN_LED_1, MCP23X17_GPIO_OUTPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_PIN_LED_2, MCP23X17_GPIO_OUTPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_PIN_LED_3, MCP23X17_GPIO_OUTPUT);

	mcp23x17_set_mode(&mcp23x17, GPIO_MODEM_PWR, MCP23X17_GPIO_OUTPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_BUZZER, MCP23X17_GPIO_OUTPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_LCD_LIGHT, MCP23X17_GPIO_OUTPUT);

	//9 teclas. Input mode
	mcp23x17_set_mode(&mcp23x17, GPIO_KEY_0, MCP23X17_GPIO_INPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_KEY_1, MCP23X17_GPIO_INPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_KEY_2, MCP23X17_GPIO_INPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_KEY_3, MCP23X17_GPIO_INPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_KEY_4, MCP23X17_GPIO_INPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_KEY_5, MCP23X17_GPIO_INPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_KEY_6, MCP23X17_GPIO_INPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_KEY_7, MCP23X17_GPIO_INPUT);
	mcp23x17_set_mode(&mcp23x17, GPIO_KEY_8, MCP23X17_GPIO_INPUT);

	//Establece el modo de un interruptor.
	mcp23x17_set_int_out_mode(&mcp23x17, MCP23X17_ACTIVE_LOW);			//Cuando se active la interrupcion, los pins se pondran a low

	uint16_t mask = (
			(1 << GPIO_KEY_0) |
			(1 << GPIO_KEY_1) |
			(1 << GPIO_KEY_2) |
			(1 << GPIO_KEY_3) |
			(1 << GPIO_KEY_4) |
			(1 << GPIO_KEY_5) |
			(1 << GPIO_KEY_6) |
			(1 << GPIO_KEY_7) |
			(1 << GPIO_KEY_8)
	);

	//Cuando los bits de la m�scara especificada se pongan en cualquier edge (high or low edge), saltar� la interrupci�n
	mcp23x17_port_set_interrupt(&mcp23x17, mask, MCP23X17_INT_ANY_EDGE);

	uint8_t buf;

	//Force mirror pin on REG_IOCON directy over the I2C
	I2C_DEV_TAKE_MUTEX(&mcp23x17);
	I2C_DEV_CHECK(&mcp23x17, i2c_dev_read_reg(&mcp23x17, 0x0A, &buf, 1));
	buf = (buf & ~(1<<(6))) | (1 ? (1<<(6)) : 0);
	I2C_DEV_CHECK(&mcp23x17, i2c_dev_write_reg(&mcp23x17, 0x0A, &buf, 1));
	I2C_DEV_GIVE_MUTEX(&mcp23x17);

	// Enable IRQ for GPIO expander		IRQ = interrupt request
	gpio_config_t io_conf;
	//interrupt of rising edge
	io_conf.intr_type = GPIO_INTR_NEGEDGE;				//Interrupci�n cuando est� en un falling edge
	//bit mask of the pins, use GPIO4/5 here
	io_conf.pin_bit_mask = (1ULL<<GPIO_EXP_IRQ_PIN);
	//set as input mode
	io_conf.mode = GPIO_MODE_INPUT;
	//disable pull-up mode and pull-down mode
	io_conf.pull_up_en = 0;
	io_conf.pull_down_en = 0;
	gpio_config(&io_conf);

	//create a queue to handle gpio event from isr			ISR = interrupt service routine
	gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));

	//install gpio isr service
	gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
	//hook isr handler for specific gpio pin
	gpio_isr_handler_add(GPIO_EXP_IRQ_PIN, gpio_isr_handler, (void*) GPIO_EXP_IRQ_PIN);		//LLama a la funcion "gpio_isr_handler" enviando el "GPIO_EXP_IRQ_PIN" pin

	ESP_LOGI(TAG, "IO initialized");

	return ESP_OK;
}

time_t timeScreen = 0;

void board_io_loop(void){

	timeScreen = time(NULL);

	while(1){

		uint32_t io_num;
		uint16_t port_value;
		int val=0;
		uint16_t key_mapping;

		if(xQueueReceive(gpio_evt_queue, &io_num, 0)) {			//Recibo un item de la cola con handler "gpio_evt_queue" y pego su valor en el buffer "io_num"

			// GPIO Expander IRQ
			if(io_num == GPIO_EXP_IRQ_PIN){		//Si el valor que he recibido de la cola es el pin "GPIO_EXP_IRQ_PIN" significa que he recibido una interrupci�n
				timeScreen = time(NULL);		//Cada vez que pulsemos alguna tecla, actualizamos la hora de la ultima pulsacion
				mcp23x17_get_level(&mcp23x17, GPIO_LCD_LIGHT, val);	//Leemos el valor de la LCD Light
				if(val==0) board_io_set_lcd_light(1);	//Si esta a 0 (luz apagada), la encendemos porque acabamos de pulsar una tecla
				if(mcp23x17_port_read(&mcp23x17, &port_value) == ESP_OK){	//To clear interrupt, a port read must be done. Lee el estado del chip mcp23x17, y si se hace correctamente:
					__io_key_detect(port_value);
				}
			}
		}
		SLEEP_MS(50);
	}
}

void board_io_set_buzzer(uint8_t value){
	uint8_t _val = (value) ? 1 : 0;
	mcp23x17_set_level(&mcp23x17, GPIO_BUZZER, _val);
}

void board_io_set_modem_pwr(uint8_t value){
	uint8_t _val = (value) ? 1 : 0;
	mcp23x17_set_level(&mcp23x17, GPIO_MODEM_PWR, _val);
}

void board_io_set_lcd_light(uint8_t value){
	uint8_t _val = (value) ? 1 : 0;			//Si value es true, val = 1. Si value es false, val = 0
	mcp23x17_set_level(&mcp23x17, GPIO_LCD_LIGHT, _val);	//Pon el device "mcp23x17" con pin "GPIO_LCD_LIGHT" a valor "_val"
}

void board_io_set_modem_power(uint8_t value){
	uint8_t aux = (value) ? 1 : 0;
	mcp23x17_set_level(&mcp23x17, EXPANSOR_GPIO_MODEM_PWR, aux);
}

void board_io_set_led(uint8_t num, uint8_t value){

	uint8_t _val = (value) ? 1 : 0;

	switch(num){		//Dependiendo del n�mero (num) de led que queramos encender o apagar:
	case 0:
		mcp23x17_set_level(&mcp23x17, GPIO_PIN_LED_0, _val);	//cambia el valor del LED 0 al estado "val"
		break;
	case 1:
		mcp23x17_set_level(&mcp23x17, GPIO_PIN_LED_1, _val);
		break;
	case 2:
		mcp23x17_set_level(&mcp23x17, GPIO_PIN_LED_2, _val);
		break;
	case 3:
		mcp23x17_set_level(&mcp23x17, GPIO_PIN_LED_3, _val);
		break;
	default:
		break;
	}
}

// ISABEL: OJO!!! Aqu� se apaga el m�dem
void board_io_set_leds(uint8_t value)
{
	uint16_t _val = 0;

	// mcp23x17_port_read(&mcp23x17, &_val);
	if (value & 0x0001) _val |= (0x01 << GPIO_PIN_LED_0);
	else _val &= ~(0x01 << GPIO_PIN_LED_0);
	if (value & 0x0002) _val |= (0x01 << GPIO_PIN_LED_1);
	else _val &= ~(0x01 << GPIO_PIN_LED_1);
	if (value & 0x0004) _val |= (0x01 << GPIO_PIN_LED_2);
	else _val &= ~(0x01 << GPIO_PIN_LED_2);
	if (value & 0x0008) _val |= (0x01 << GPIO_PIN_LED_3);
	else _val &= ~(0x01 << GPIO_PIN_LED_3);
	if (value & 0x0010) _val |= (0x01 << GPIO_LCD_LIGHT);
	else _val &= ~(0x01 << GPIO_LCD_LIGHT);
	if (value & 0x0020) _val |= (0x01 << GPIO_MODEM_PWR);
	else _val &= ~(0x01 << GPIO_MODEM_PWR);
	mcp23x17_port_write(&mcp23x17, _val);
}


static void gpio_isr_handler(void* arg)
{
	uint32_t gpio_num = (uint32_t) arg;
	xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);		//Envia el valor de gpio_num a la cola
}


#define KEYBOARD_PORT_CHECK(NUM)																\
		if(port_data & (1<<GPIO_KEY_##NUM)){ BIT_SET(io_keyboard_new_status, NUM, uint16_t); }	\
		else{ BIT_CLR(io_keyboard_new_status, NUM, uint16_t); }

void __io_key_detect(uint16_t port_data){

	uint16_t io_keyboard_new_status = 0;

	KEYBOARD_PORT_CHECK(0);
	KEYBOARD_PORT_CHECK(1);
	KEYBOARD_PORT_CHECK(2);
	KEYBOARD_PORT_CHECK(3);
	KEYBOARD_PORT_CHECK(4);
	KEYBOARD_PORT_CHECK(5);
	KEYBOARD_PORT_CHECK(6);
	KEYBOARD_PORT_CHECK(7);
	KEYBOARD_PORT_CHECK(8);

	for(uint8_t k = 0; k < GPIO_KEY_MAX; k++){
		if(BIT_IS_SET(io_keyboard_new_status, k, uint16_t) != BIT_IS_SET(io_keyboard_last_status, k, uint16_t)){	//Si el estado de la tecla ha cambiado: checkea su valor actual y tiempo de pulsaci�n

			// If 0 means key was pressed
			if(!(BIT_IS_SET(io_keyboard_new_status, k, uint16_t))){		//Si el estado de cada uno de las 9 teclas (barrido desde k=0 a k=8) es igual a 0 (tecla pulsada):
				uint64_t pressed_time = 0;		//Obtiene el tiempo de pulsaci�n. Tiempo pulsado = tiempo actual - tiempo inicial
				esp_event_post(EVENTS_KEYBOARD, EVENTS_KEYBOARD_KEY_1 + k, &pressed_time, sizeof(uint64_t), pdMS_TO_TICKS(200) );	//Envia el tiempo de pulsaci�n
				io_keyboard_times[k] = (esp_timer_get_time() / 1000);	//Obtiene el tiempo actual
			}

			// If 1 means key was release
			if(BIT_IS_SET(io_keyboard_new_status, k, uint16_t)){		//Si el estado de cada uno de las 9 teclas (barrido desde k=0 a k=8) es igual a 1 (tecla soltada):
				uint64_t pressed_time = ((esp_timer_get_time() / 1000) - io_keyboard_times[k]);		//Obtiene el tiempo de pulsaci�n. Tiempo pulsado = tiempo actual - tiempo inicial
				esp_event_post(EVENTS_KEYBOARD, EVENTS_KEYBOARD_NO_KEY_1 + k, &pressed_time, sizeof(uint64_t), pdMS_TO_TICKS(200) );	//Envia el tiempo de pulsaci�n
				io_keyboard_times[k] = (esp_timer_get_time() / 1000);
			}
		}
	}

	io_keyboard_last_status = io_keyboard_new_status;		//Actualiza el estado de las teclas (0=pulsada, 1=no pulsada)
}
