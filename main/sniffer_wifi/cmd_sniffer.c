/* From cmd_sniffer example: That example code was in the Public Domain (or CC0 licensed, at your option.)

   Changes by Rafael Gandia-Castello

*/
#include "../sniffer_wifi/cmd_sniffer.h"

#include <string.h>
#include "argtable3/argtable3.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/event_groups.h"
#include <sys/unistd.h>
#include <sys/fcntl.h>
#include "esp_log.h"
#include "esp_wifi.h"
#include "esp_console.h"
#include "esp_app_trace.h"
#include "sdkconfig.h"
#include "stdint.h"
#include "string.h"
#include "esp_timer.h"
#include "esp_log.h"
//#include "timer_group_example_main.c"


#include "../sniffer_wifi/listamacs.h"
//#include "../sniffer_wifi/listamacs.h"
#include "../sniffer_wifi/pcap.h"

#define SNIFFER_DEFAULT_FILE_NAME "esp-sniffer"
#define SNIFFER_FILE_NAME_MAX_LEN CONFIG_PCAP_FILE_NAME_MAX_LEN
#define SNIFFER_DEFAULT_CHANNEL (1)
#define SNIFFER_PAYLOAD_FCS_LEN (4)
#define SNIFFER_PROCESS_PACKET_TIMEOUT_MS (100)
#define SNIFFER_PROCESS_APPTRACE_TIMEOUT_US (100)
#define SNIFFER_APPTRACE_RETRY  (10)

bool nuevoUsuario = false;		//Indica si hay un nuevo usuario o no. Es el booleano de salida hacia el main.c del proyecto

uint8_t pruebaMAC[6] = {0x18,0xFE,0x34,0x00,0x00,0x12};
uint8_t MySA[40][6];
uint8_t MySA[40][6];

static const char *SNIFFER_TAG = "cmd_sniffer";
#define SNIFFER_CHECK(a, str, goto_tag, ...)                                              \
    do                                                                                    \
    {                                                                                     \
        if (!(a))                                                                         \
        {                                                                                 \
            ESP_LOGE(SNIFFER_TAG, "%s(%d): " str, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
            goto goto_tag;                                                                \
        }                                                                                 \
    } while (0)

typedef struct {
    char *filter_name;
    uint32_t filter_val;
} wlan_filter_table_t;

typedef struct {
    bool is_running;
    sniffer_intf_t interf;
    uint32_t channel;
    uint32_t filter;
    pcap_handle_t pcap;
} sniffer_runtime_t;

typedef struct {
    void *payload;
    uint32_t length;
    uint32_t seconds;
    uint32_t microseconds;
} sniffer_packet_info_t;

typedef struct {
     unsigned frame_ctrl:16;
     unsigned duration_id:16;
     uint8_t DA[6]; /* receiver address */
     uint8_t SA[6]; /* sender address */
     uint8_t addr3[6]; /* filtering address */
     unsigned sequence_ctrl:16;
     uint8_t addr4[6]; /* optional */
  } wifi_ieee80211_mac_hdr_t;


#if CONFIG_SNIFFER_PCAP_DESTINATION_MQTT
    char virtbuf[1024];
    FILE* virtfile;
#endif

static sniffer_runtime_t snf_rt = {0};
static wlan_filter_table_t wifi_filter_hash_table[SNIFFER_WLAN_FILTER_MAX] = {0};
// Para guardar el ruido en los canales
float* arrayruido;

#define maxCanal 12

long int total[maxCanal];
long int num[maxCanal];

void addNumFilter(int canal,int valor) {
  total[canal]+=valor;
  num[canal]++;
}

void resultadoFiltro(float* resul) {
  for(int i=0;i<maxCanal;i++) {
      if (num[i]==0) resul[i]=0;
      else resul[i]=total[i]/num[i];
  }
}

long int contador = 0;
bool espera = false;


node_t espressif[80];
int espressifMACs[72][3] = {{0x08,0x3A,0xF2},{0x0C,0xDC,0x7E},{0x10,0x52,0x1C},{0x18,0xFE,0x34},{0x24,0x0A,0xC4},{0x24,0x62,0xAB},{0x24,0x6F,0x28},{0x24,0xA1,0x60},{0x24,0xB2,0xDE},{0x2C,0x3A,0xE8},
						   {0x2C,0xF4,0x32},{0x30,0x83,0x98},{0x30,0xAE,0xA4},{0x34,0x86,0x5D},{0x34,0xAB,0x95},{0x3C,0x61,0x05},{0x3C,0x71,0xBF},{0x40,0xF5,0x20},{0x48,0x3F,0xDA},{0x4C,0x11,0xAE},
						   {0x4C,0x75,0x25},{0x50,0x02,0x91},{0x54,0x5A,0xA6},{0x5C,0xCF,0x7F},{0x60,0x01,0x94},{0x60,0x55,0xF9},{0x68,0xC6,0x3A},{0x70,0x03,0x9F},{0x78,0xEF,0x6D},{0x7C,0x9E,0xBD},
						   {0x7C,0xDF,0xA1},{0x80,0x7D,0x3A},{0x84,0x0D,0x8E},{0x84,0xCC,0xA8},{0x84,0xF3,0xEB},{0x8C,0xAA,0xB5},{0x8C,0xCE,0x4E},{0x90,0x97,0xD5},{0x94,0x3C,0xC6},{0x94,0xB9,0x7E},
						   {0x98,0xCD,0xAC},{0x98,0xF4,0xAB},{0x9C,0x9C,0x1F},{0xA0,0x20,0xA6},{0xA0,0x76,0x4E},{0xA4,0x7B,0x9D},{0xA4,0xCF,0x12},{0xA4,0xE5,0x7C},{0xA8,0x03,0x2A},{0xAC,0x67,0xB2},
						   {0xAC,0xD0,0x74},{0xB4,0xE6,0x2D},{0xB8,0xF0,0x09},{0xBC,0xDD,0xC2},{0xBC,0xFF,0x4D},{0xC4,0x4F,0x33},{0xC4,0x5B,0xBE},{0xC4,0xDD,0x57},{0xC8,0x2B,0x96},{0xCC,0x50,0xE3},
						   {0xD8,0xA0,0x1D},{0xD8,0xBF,0xC0},{0xD8,0xF1,0x5B},{0xDC,0x4F,0x22},{0xE0,0x98,0x06},{0xE0,0xE2,0xE6},{0xE8,0x68,0xE7},{0xE8,0xDB,0x84},{0xEC,0xFA,0xBC},{0xF0,0x08,0xD1},
						   {0xF4,0xCF,0x02},{0xFC,0xF5,0xC4}
																																									};


void espressifMACsList (void){		//Pega la array espressifMACs[4][3] en la estructura espressif, la cual almacena todas las MACs pertenecientes a espressif

	for(int i=0;i<72;i++){

		for(int j=0;j<3;j++){

			memcpy(&espressif[i].mac[j],&espressifMACs[i][j],1);

		}
/*
  	  printf("MAC[%i]= %02X:%02X:%02X:%02X:%02X:%02X\n",i,espressif[i].mac[0],espressif[i].mac[1],espressif[i].mac[2],espressif[i].mac[3],espressif[i].mac[4],espressif[i].mac[5]);
*/
	}

}


bool espressifRangeMAC(uint8_t* mac){		//Compara la mac que se le introduce de entrada con todas las MACs de espressif para ver si pertenece a la lista o no

	int lista = 0;

	for(int i=0;i<72;i++){		//Passamos por cada una de las MACs de espressif

		for(int j=0;j<3;j++){		//Pasamos por cada uno de los 3 bytes de cada MAC

			if(mac[j] == espressif[i].mac[j]) lista++;		//Si un byte coincide, incrementamos

		}

		if(lista == 3) {

		   //printf("Hemos detectado una MAC de Espressif. MAC =  %02X:%02X:%02X:%02X:%02X:%02X\n",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);

		   return true;		//Si coinciden 3 bytes, devolvemos true, la mac identificada es parte de la lista de espressif
		}
		else lista = 0;					//Y si no, reseteamos el contador
	}

	return false;

}

bool fakeMAC (uint8_t* mac){	//Si las macs sniffeadas contienen 2, 6, A, o E en la segunda posicion, son macs fakes dadas por usuarios reales

	int output;

	output = mac[0] & 0b00001111;

	if (output == 0b0010 || output == 0b0110 || output == 0b1010 || output == 0b1110) return true;		//2, 6, A, E

	else return false;

}

static void wifi_sniffer_cb(void *recv_buf, wifi_promiscuous_pkt_type_t type)
{

	uint8_t *mac;
    wifi_promiscuous_pkt_t *sniffer = (wifi_promiscuous_pkt_t *)recv_buf;
    //type != WIFI_PKT_MISC

      if (!sniffer->rx_ctrl.rx_state) {	//Si no son mensajes recibidos, es decir, si solo los mensajes son enviados por parte del usuario detectado

		  addNumFilter(sniffer->rx_ctrl.channel,sniffer->rx_ctrl.noise_floor);
		  wifi_ieee80211_mac_hdr_t *cac=sniffer->payload;
		  mac = cac->SA;

    	  if(!espressifRangeMAC(mac)){	//Si no es una mac de espressif, vamos bien. Si fuera una MAC de espressif, la descartamos ya (NO es un usuario real detectado)

    		  if(fakeMAC(mac)){	//Si encontramos una MAC fake, es un usuario real 100 % (los usuarios reales dan una MAC fake, no su mac verdadera)

    			  //Guarda la MAC y el RSSI de los usuarios. Esta funcion controla que no se guardan usuarios repetidos. Si detecta al mismo usuario varias veces, actualiza su estado (tiempo, rssi y count)
    			  processMAC(mac,sniffer->rx_ctrl.rssi,sniffer->rx_ctrl.noise_floor);
    			  //ESP_LOGI("SNIFFER", "USUARIO DETECTADO -> MAC: %02X:%02X:%02X:%02X:%02X:%02X | RSSI: %d | Noise: %d | SNR: %d",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5],sniffer->rx_ctrl.rssi,sniffer->rx_ctrl.noise_floor,sniffer->rx_ctrl.rssi-sniffer->rx_ctrl.noise_floor);
    			  nuevoUsuario = true;
    			  vTaskDelay(100 / portTICK_PERIOD_MS);

    		  }
    	  }
    	  //checkHayUsuario(); //Checkea si del buffer de 500 macs posibles, queda alguna registrada o no. Si no la hay, no hay usuario cerca
      }

}
//char mac[29]="{\"mac\":\"11:22:33:44:55:66\"}XX";

void printMyHeap() {
//	xPortGetFreeHeapSize();// is a FreeRTOS function which returns the number of free bytes in the (data memory) heap. This is equivalent to calling heap_caps_get_free_size(MALLOC_CAP_8BIT).
//	heap_caps_get_free_size(); // can also be used to return the current free memory for different memory capabilities.
//	heap_caps_get_largest_free_block(); // can be used to return the largest free block in the heap. This is the largest single allocation which is currently possible. Tracking this value and comparing to total free heap allows you to detect heap fragmentation.
//	xPortGetMinimumEverFreeHeapSize();// and the related heap_caps_get_minimum_free_size() can be used to track the heap â€œlow water markâ€� since boot.
//	heap_caps_get_info();// returns a multi_heap_info_t structure which contains the information from the above functions, plus some additional heap-specific data (number of allocations, etc.).
	heap_caps_print_heap_info(MALLOC_CAP_32BIT | MALLOC_CAP_8BIT); // prints a summary to stdout of the information returned by heap_caps_get_info().

	/*
	heap_caps_dump(); // and
	heap_caps_dump_all(); // will output detailed information about the structure of each block in the heap. Note that this can be large amount of output.
	*/
}

wifi_promiscuous_filter_t wifi_filter;

int snifferTaskFail=0;
pcap_config_t pcap_config;

esp_err_t do_sniffer_stop() {
	/* stop sniffer */
//	pcap_deinit(snf_rt.pcap);
	ESP_LOGI(SNIFFER_TAG,"Canal %i - NF:%.0f\n",snf_rt.channel,(float)total[snf_rt.channel]/(float)num[snf_rt.channel]);
//	ESP_LOGI(SNIFFER_TAG,"AAAAAA: %d",sizeof(wifi_pkt_rx_ctrl_t));
    SNIFFER_CHECK(snf_rt.is_running, "sniffer is already stopped", err);
    SNIFFER_CHECK(esp_wifi_set_promiscuous(false) == ESP_OK, "stop wifi promiscuous failed", err);
    snf_rt.is_running = false;
    return ESP_OK;
err:
    snifferTaskFail++;
    return ESP_FAIL;

}
void do_sniffer_start(int channel,float* arraynoise) {
    arrayruido=arraynoise;

    printMyHeap();
    //if (reservaSNIFF!=NULL) free(reservaSNIFF);
    snf_rt.channel=channel;
    switch (snf_rt.interf) {
    case SNIFFER_INTF_WLAN:
        /* Start WiFi Promicuous Mode */
        wifi_filter.filter_mask = snf_rt.filter;
        esp_wifi_set_promiscuous_filter(&wifi_filter);			//Filtra todos los mensajes excepto los que est�n en modo promiscuo
        esp_wifi_set_promiscuous_rx_cb(wifi_sniffer_cb);		//Registra callback. Cada vez que se reciba un paquete, llama al callback
        SNIFFER_CHECK(esp_wifi_set_promiscuous(true) == ESP_OK, "start wifi promiscuous failed", err_start);
        snf_rt.is_running = true;
        //esp_wifi_set_channel(snf_rt.channel, WIFI_SECOND_CHAN_NONE);
        ESP_LOGI(SNIFFER_TAG, "start WiFi promiscuous ok");
        break;
    default:
        break;
    }
    //return ESP_OK;
err_start:
	snifferTaskFail++;
	ESP_LOGI(SNIFFER_TAG, "Listo para reiniciar");
	if (snifferTaskFail>3) {
		ESP_LOGI(SNIFFER_TAG, "REINICIANDO");
		esp_restart();
	}
	 //return ESP_FAIL;
}

FILE *	virtfile;
int virtbuf;
void sniffInit() {

	espressifMACsList();		//Copia las MACs de espressif

	//if(espressifRangeMAC(pruebaMAC)) printf("Mac espressif encontrada \n");
	virtfile = fmemopen(virtbuf,1024,"r+");

	/* stop sniffer */
	snf_rt.interf = SNIFFER_INTF_WLAN;
	snf_rt.channel = 1;
	snf_rt.filter = WIFI_PROMIS_FILTER_MASK_ALL;
	//sniffer_start(&snf_rt);

    switch (snf_rt.interf) {
    case SNIFFER_INTF_WLAN:
        pcap_config.link_type = PCAP_LINK_TYPE_802_11;
        break;
    default:
        SNIFFER_CHECK(false, "unsupported interface", err);
        break;
    }

    /* Create file to write, binary format */
    // TODO: Aqui esta el tipo FILE que se abre para escribir los datos!!
    // La funcion pcap_init usa ese handle para hacer los writes
    pcap_config.fp=virtfile;
    SNIFFER_CHECK(pcap_config.fp, "open file failed", err);
    ESP_LOGI(SNIFFER_TAG, "open file successfully");

    /* init a pcap session */
    SNIFFER_CHECK(pcap_init(&pcap_config, &snf_rt.pcap) == ESP_OK, "init pcap session failed", err);
    snf_rt.is_running = true;
    return;
err:
	ESP_LOGE(SNIFFER_TAG, "Fallo al inicializar el SNIFFER");
    return;
}

