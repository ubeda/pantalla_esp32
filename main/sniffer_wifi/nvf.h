/*
 * jsonconfig.h
 *
 *  Created on: 25 dic. 2018
 *      Author: dismuntel
 */

#ifndef NVF_H_
#define NVF_H_

#define STORAGE_NAMESPACE "dismuntel"

char* getMAC(char* macstr);

void setDataDefault();
//void setParamToDefault();

//void readNVS(int inc);
//esp_err_t writeNVS(void);
esp_err_t writeNVSdatos(void) ;

extern bool emparejamiento;

void emparejar(uint8_t* mac);
bool emparejados(uint8_t* mac);
void cambiapareja(uint8_t* macble,uint8_t* macrand);
void borraremparejados();

typedef struct { // DESDE FUERA SE LEEN Y SE ESCRIBEN
	char name[30];
    char ssid[30];		// SSID del Wifi al que se conectará como cliente (STA)
    char psswd[60];		// PASSWORD del Wifi al que se conectará como cliente (STA)
	int8_t Temperatura;
} param;

// For now, we assume that flash sector size is 4096
// 4096 / 10 bytes = ~400 => 400*5min/60min = 33h ==> 100000/ (12*24) = 347dias
// 4096 / 10 bytes = ~400 => 400*10min/60min = 66h ==> 100000/ (6*24) = 694dias
struct iot_t {
	time_t fechahora;
	int8_t Temp1;
	int8_t Temp2;
} __attribute__((packed));
struct niot_datos {
	struct iot_t dato[2000];
} __attribute__((packed));

#define maxMAC 10
typedef struct {
	uint8_t mac[6];
} mimac_t;
typedef struct {
	mimac_t mac[maxMAC];
} sw_macs;

extern param nodoiotParam;
extern struct niot_datos nodoiotDatos;
extern sw_macs nodoiotMACs;

#endif /* NVF_H_ */
