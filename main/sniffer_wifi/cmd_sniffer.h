/* From cmd_sniffer example: That example code was in the Public Domain (or CC0 licensed, at your option.)

   Changes by Rafael Gandia-Castello
*/
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Supported Sniffer Interface
 *
 */
#include "stdbool.h"
#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include "esp_err.h"

extern bool espera;
extern bool nuevoUsuario;


typedef enum {
    SNIFFER_INTF_WLAN = 0, /*!< WLAN interface */
} sniffer_intf_t;

/**
 * @brief WLAN Sniffer Filter
 *
 */
typedef enum {
    SNIFFER_WLAN_FILTER_MGMT = 0, /*!< MGMT */
    SNIFFER_WLAN_FILTER_CTRL,     /*!< CTRL */
    SNIFFER_WLAN_FILTER_DATA,     /*!< DATA */
    SNIFFER_WLAN_FILTER_MISC,     /*!< MISC */
    SNIFFER_WLAN_FILTER_MPDU,     /*!< MPDU */
    SNIFFER_WLAN_FILTER_AMPDU,    /*!< AMPDU */
    SNIFFER_WLAN_FILTER_MAX
} sniffer_wlan_filter_t;

void espressifMACsList (void);
bool espressifRangeMAC(uint8_t* mac);
bool fakeMAC (uint8_t* mac);
void register_sniffer();
char* setWifiTrackerMAC(const char* topicfin);

esp_err_t do_sniffer_stop();
void do_sniffer_start(int channel,float* arraynoise);

void initFilter(int* canales,long int* Num);
void resultadoFiltro(float* resul);

void sniffInit();

extern uint8_t miChannel;

#ifdef __cplusplus
}
#endif
