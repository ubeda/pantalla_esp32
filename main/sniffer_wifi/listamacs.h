/*
 * listamacs.h
 *
 *  Created on: 5 jun. 2020
 *      Author: rafa400
 */

#ifndef MAIN_LISTAMACS_H_
#define MAIN_LISTAMACS_H_

#define MAXFROM 5
#define MAXMACS 500
#define TIEMPODECADUCIDAD 6000  // En segundos (10 min * 60 s)

#include "esp_mesh.h"


typedef struct nodes {
    uint8_t mac[6];
    int8_t rssi;
	uint16_t detectada; // =esp_timer_get_time()/1000000; // El tiempo por defecto hasta que se aborta son 00:04:05
	uint16_t count;
	int8_t noise;
	int8_t snr;
} node_t;


typedef struct nodesMain {
    uint8_t mac[6];
    int8_t rssi[MAXFROM];
    uint16_t detectada; // =esp_timer_get_time()/1000000; // El tiempo por defecto hasta que se aborta son 00:04:05
    uint16_t count[MAXFROM];
} nodeMain_t;


extern node_t misMACS[MAXMACS];
extern nodeMain_t misMainMACS[MAXMACS];

void initMAC();
void processMAC(uint8_t* mac,int8_t rssi, int8_t noise);
void processMAC1(uint8_t* mac,int8_t rssi);
void tengoMAC(uint8_t* mac,int8_t rssi,int channel,wifi_promiscuous_pkt_type_t type,uint8_t payload);
void checkHayUsuario (void);
float calculaSize(void);
void printMAC();
void printRemMAC(node_t *unasMAC);
void mainListaMAC(node_t *unasMAC,int num);
void printMainMAC();
int printLineaMAC(char* buffer,int i);
void actualizaUsuario(void);
int listaFrom(mesh_addr_t from);

#endif /* MAIN_LISTAMACS_H_ */
