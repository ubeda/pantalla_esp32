/*
 * listamacs.c
 *
 *  Created on: 5 jun. 2020
 *      Author: rafa400
 */


#include "../sniffer_wifi/listamacs.h"

#include "stdint.h"
#include "string.h"
#include "esp_timer.h"
#include "esp_log.h"
#include "cmd_sniffer.h"


// Lista de MACs que monitorizamos
node_t misMACS[MAXMACS];
node_t nuevos[MAXMACS];
nodeMain_t misMainMACS[MAXMACS];
float misRSSI[MAXMACS];
float misRSSInuevos[MAXMACS];
int lastTime=0;
uint8_t ceromac[6]={0,0,0,0,0,0};
// Lista de nodos MESH que monitorizan y reportan
mesh_addr_t fromList[MAXFROM];
int fromN=1; // el 0 es el nodo ROOT
int repetida = 0;	//Si repetida es 0, significa que hay una mac nueva detectada. Si repetida es mayor que 0, significa que ya la mac detectada ya la teniamos guardada de antes (ya se habia detectado en el barrido)

void initMAC() {
	memset(&(misMACS[0]),0,sizeof(node_t)*MAXMACS);
	memset(misRSSI,0,sizeof(misRSSI));

	memset(&(nuevos[0]),0,sizeof(node_t)*MAXMACS);
	memset(misRSSInuevos,0,sizeof(misRSSInuevos));
}

void processMAC1(uint8_t* mac,int8_t rssi) {
	int actualtime=esp_timer_get_time()/1000000;
	for(int i=0;i<MAXMACS;i++) {
		if (memcmp(mac,&nuevos[i],6)==0) {		//Si ya tengo la MAC guardada, actualiza sus valores (tiempo, rssi y n�mero de paquetes recibidos)
			nuevos[i].detectada=actualtime;
			misRSSInuevos[i]+=rssi;
			nuevos[i].count++;											//Acumula el n�mero de paquetes recibidos por esa mac
			nuevos[i].rssi=(misRSSInuevos[i]/(float)nuevos[i].count);		//Calcula el rssi medio a partir del sumatorio de todos los rssi de la misma mac dividido por el n�mero de paquetes recibidos por esa mac
			break;
		} else if (memcmp(&nuevos[i],ceromac,6)==0) {		//Si alg�n campo de misMACS de los 100 posibles est� vac�o (est� libre), le guardamos el valor de la mac
			memcpy(&nuevos[i].mac,mac,6);
			nuevos[i].detectada=actualtime;
			misRSSInuevos[i]+=rssi;
			nuevos[i].count++;
			nuevos[i].rssi=(misRSSInuevos[i]/(float)nuevos[i].count);
 			break;
		} else if (actualtime - nuevos[i].detectada >TIEMPODECADUCIDAD) {		//Si durante un tiempo no detectamos esa mac, la borramos para que no ocupe espacio
			//if (i==MAXMACS-1)
				memset(&nuevos[i],0,sizeof(node_t));								//Si he llegado a la posicion final, pongo esa posicion a 0
			//else
				//memcpy(&misMACS[i],&misMACS[i+1],sizeof(node_t)*(MAXMACS-i-1));		//Si no he llegado a la posicion final, subo toda la cola hacia arriba

		}
	}
}

void processMAC(uint8_t* mac,int8_t rssi, int8_t noise) {
	int actualtime=esp_timer_get_time()/1000000;
	for(int i=0;i<MAXMACS;i++) {
		if (memcmp(mac,&misMACS[i],6)==0) {		//Si ya tengo la MAC guardada, actualiza sus valores (tiempo, rssi y n�mero de paquetes recibidos)
			misMACS[i].detectada=actualtime;
			misRSSI[i]+=rssi;
			misMACS[i].count++;											//Acumula el n�mero de paquetes recibidos por esa mac
			misMACS[i].rssi=(misRSSI[i]/(float)misMACS[i].count);		//Calcula el rssi medio a partir del sumatorio de todos los rssi de la misma mac dividido por el n�mero de paquetes recibidos por esa mac
			misMACS[i].noise = noise;
			misMACS[i].snr = rssi - noise;
			//ESP_LOGI("SNIFFER", "Usuario actualizado -> MAC: %02X:%02X:%02X:%02X:%02X:%02X | RSSI: %d | Noise: %d | SNR: %d | count: %d",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5],misMACS[i].rssi,misMACS[i].noise,misMACS[i].snr,misMACS[i].count);
			break;
		} else if (memcmp(&misMACS[i],ceromac,6)==0) {		//Si NO tengo la mac guardada y adem�s alg�n campo de misMACS de los 500 posibles est� vac�o (est� libre), registramos la mac
			memcpy(&misMACS[i].mac,mac,6);
			misMACS[i].detectada=actualtime;
			misRSSI[i]+=rssi;
			misMACS[i].count++;
			misMACS[i].rssi=(misRSSI[i]/(float)misMACS[i].count);
			misMACS[i].noise = noise;
			misMACS[i].snr = rssi - noise;
			ESP_LOGW("SNIFFER", "Nuevo usuario -> MAC: %02X:%02X:%02X:%02X:%02X:%02X | RSSI: %d | Noise: %d | SNR: %d | count: %d",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5],misMACS[i].rssi,misMACS[i].noise,misMACS[i].snr,misMACS[i].count);
 			break;
		} else if (actualtime - misMACS[i].detectada >TIEMPODECADUCIDAD) {		//Si durante un tiempo no detectamos esa mac, la borramos para que no ocupe espacio
			memset(&misMACS[i],0,sizeof(node_t));								//Si he llegado a la posicion final, pongo esa posicion a 0

		}
	}
}

//Checkea si hace tiempo que no ha detectado a los usuarios guardados. Si es as�, borra su mac. En caso de borrar todas las mac, NO habr�a usuario cerca
//Ademas checkea si del buffer de 500 macs posibles, queda alguna registrada o no (si no es 0). Si no la hay, no hay usuario cerca

void actualizaUsuario(void){
	int currentTime=esp_timer_get_time()/1000000;
	int counter = 0;
	for(int i=0;i<MAXMACS;i++) {
		if (currentTime - misMACS[i].detectada >TIEMPODECADUCIDAD) { //Si durante un tiempo no detectamos esa mac, la borramos porque siginifica que ese usuario ya no esta cerca
			memset(&misMACS[i],0,sizeof(node_t));	//Borramos su mac
		}
		if (memcmp(&misMACS[i],ceromac,6)==0){
			counter++;
		}
	}
	if (counter == 500) nuevoUsuario = false;	//Si tengo el buffer de 500 macs completamente vacio, no hay usuario
	else nuevoUsuario = true;	//Si hay al menos una mac guardada, si que hay usuario
}

//Obsoleta. Esta integrada en actualizaUsuario(void)
void checkHayUsuario (void) {	//Checkea si del buffer de 500 macs posibles, queda alguna registrada o no. Si no la hay, no hay usuario cerca
	int counter = 0;
	for(int i=0;i<MAXMACS;i++) {
		if (memcmp(&misMACS[i],ceromac,6)==0){
			counter++;
		}
	}

	if (counter == 500) nuevoUsuario = false;	//Si tengo el buffer de 500 macs completamente vacio, no hay usuario
	else nuevoUsuario = true;	//Si hay al menos una mac guardada, si que hay usuario

}

float calculaSize(void){
	float size = 0;

	for(int i=0;i<MAXMACS;i++) {
		if (memcmp(&nuevos[i],ceromac,6) != 0) size ++;

	}

return size;
}


void tengoMAC(uint8_t* mac,int8_t rssi,int channel,wifi_promiscuous_pkt_type_t type,uint8_t payload){	//Compara la mac detectada con las macs guardadas, si no la teniamos guardada, la muestra
	float Size;
	for(int i=0;i<MAXMACS;i++) {

		if(memcmp(mac,&misMACS[i],6) == 0) repetida++;   //Si la mac de entrada es igual a una de las macs guardadas, se incrementa repetida

		if(repetida == 0 && i==(MAXMACS-1)) { 		//Si repetida no se ha incrementado y sigue siendo 0, significa que la mac no est� repetida y por lo tanto es una nueva mac que no teniamos guardada
			processMAC1(mac,rssi);					//Es una nueva mac, es decir un nuevo usuario real detectado, y me lo guardo en mi segundo objeto nuevos[MAXMACS]
			processMAC(mac,rssi,NULL);					//Hay que borrarlo (lo tenemos ahora par no saturar el log porque en dismuntel hay muchos emisores wifi)
	  		Size = calculaSize();
			ESP_LOGI("SNIFFER", "Tenemos %.0f usuarios unicos detectados",Size);

			switch(type){

			case WIFI_PKT_MGMT:
				ESP_LOGI("SNIFFER", "NUEVO USUARIO -> MAC: %02X:%02X:%02X:%02X:%02X:%02X | RSSI: %d | Channel = %d | Payload = %02X | Type = MGMT",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5],rssi,channel,payload);
				break;

			case WIFI_PKT_CTRL:
				ESP_LOGI("SNIFFER", "NUEVO USUARIO -> MAC: %02X:%02X:%02X:%02X:%02X:%02X | RSSI: %d | Channel = %d | Payload = %02X | Type = CTRL",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5],rssi,channel,payload);
				break;

			case WIFI_PKT_DATA:
				ESP_LOGI("SNIFFER", "NUEVO USUARIO -> MAC: %02X:%02X:%02X:%02X:%02X:%02X | RSSI: %d | Channel = %d | Payload = %02X | Type = DATA",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5],rssi,channel,payload);
				break;

			case WIFI_PKT_MISC:
				ESP_LOGI("SNIFFER", "NUEVO USUARIO -> MAC: %02X:%02X:%02X:%02X:%02X:%02X | RSSI: %d | Channel = %d | Payload = %02X | Type = MISC",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5],rssi,channel,payload);
				break;

			default:
				ESP_LOGI("SNIFFER", "NUEVO USUARIO -> MAC: %02X:%02X:%02X:%02X:%02X:%02X | RSSI: %d | Channel = %d | Payload = %02X | Type = DEFAULT",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5],rssi,channel,payload);
				break;

			}

		}

	}

	repetida = 0;
}

void printMAC() {
	for(int i=0;i<MAXMACS;i++) {
		uint8_t *m=&(misMACS[i].mac[0]);
		if (memcmp(m,ceromac,6)==0) break;													//Si el campo est� vac�o (no se ha guardado mac), no se hace nada
		ESP_LOGI("SNIFFER", "USUARIO GUARDADO -> MAC: %02X:%02X:%02X:%02X:%02X:%02X | RSSI: %4d | COUNT: %5d",
					m[0],m[1],m[2],m[3],m[4],m[5],misMACS[i].rssi, misMACS[i].count);		//Imprime las macs guardadas
	}
	//initMAC();
}

void printRemMAC(node_t *unasMAC) {
	for(int i=0;i<MAXMACS;i++) {
		uint8_t *m=&(unasMAC[i].mac[0]);
		if (memcmp(m,ceromac,6)==0) break;
		ESP_LOGI("MIO", "%02X:%02X:%02X:%02X:%02X:%02X | %4d | %5d",
				m[0],m[1],m[2],m[3],m[4],m[5],unasMAC[i].rssi, unasMAC[i].count);
	}
	initMAC();
}

void mainListaMAC(node_t *unasMAC,int num) {
	for(int i=0;i<MAXMACS;i++) {
		uint8_t *m=&(unasMAC[i].mac[0]);
		if (memcmp(m,ceromac,6)==0) break;
		for(int j=0;j<MAXMACS;j++) {
			uint8_t *n=&(misMainMACS[j].mac[0]);
			if (memcmp(n,ceromac,6)==0) memcpy(n,m,6);
			if (memcmp(n,m,6)==0) {
				((nodeMain_t *)n)->rssi[num]=((node_t *)m)->rssi;
				((nodeMain_t *)n)->count[num]=((node_t *)m)->count;
				if ( ((nodeMain_t *)n)->detectada<((node_t *)m)->detectada )
					((nodeMain_t *)n)->detectada=((node_t *)m)->detectada;
				break;
			}
		}
	}
}

void printMainMAC() {
	for(int i=0;i<MAXMACS;i++) {
		uint8_t *m=&(misMainMACS[i].mac[0]);
		nodeMain_t *n=&(misMainMACS[i]);
		if (memcmp(m,ceromac,6)==0) break;
		ESP_LOGI("MIO", "%02X:%02X:%02X:%02X:%02X:%02X | %4d | %4d | %4d | %5d | %5d | %5d",
				m[0],m[1],m[2],m[3],m[4],m[5],
				n->rssi[0], n->rssi[1], n->rssi[2],
				n->count[0], n->count[1], n->count[2]);
	}
}

char *linea_json_string="{\"mac\":%2d,"
		"\"data\":{}"
		"}" ;

int printLineaMAC(char* buffer,int i) {
	uint8_t *m=&(misMainMACS[i].mac[0]);
	nodeMain_t *n=&(misMainMACS[i]);
	if (memcmp(m,ceromac,6)==0) return -1;
	return snprintf(buffer,500,"%c{\"mac\":\"%02X:%02X:%02X:%02X:%02X:%02X\",\"s1\":%d,\"s2\":%d,\"s3\":%d,\"n1\":%d,\"n2\":%d,\"n3\":%d}",
			    (i==0?' ':','),
				m[0],m[1],m[2],m[3],m[4],m[5],
				n->rssi[0], n->rssi[1], n->rssi[2],
				n->count[0], n->count[1], n->count[2]);
}

int listaFrom(mesh_addr_t from) {
	for(int i=1;i<fromN;i++) {
		if (memcmp(from.addr,fromList[i].addr,6)==0)
			return i;
	}
	if (fromN==MAXFROM) return -1;
	memcpy(fromList[fromN].addr,from.addr,6);
	fromN++;
	return fromN-1;
}
