/*
 * jsonconfig.c
 *
 *  Created on: 25 dic. 2018
 *      Author: dismuntel
 */

#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_err.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"

#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"
#include "esp_app_trace.h"

#include <time.h>

#include "sdkconfig.h"
#include "../sniffer_wifi/nvf.h"

// For now, we assume that flash sector size is 4096

static const char* NVFnodoTAG = "NodoIoT-NVF";

param nodoiotParam;
struct niot_datos nodoiotDatos;
sw_macs nodoiotMACs;

bool enmarcha=false;
bool emparejamiento=false;

int64_t timeAguaIni=0;

void setDataDefault() {
	memset(&nodoiotDatos,0,sizeof(nodoiotDatos));
	nodoiotDatos.dato[0].Temp1=0;
}
/*
void setParamToDefault(){
    borraremparejados();
    memset(&nodoiotParam,0,sizeof(nodoiotParam));
	//strcpy(nodoiotParam.ssid,CONFIG_MESH_ROUTER_SSID);
	//strcpy(nodoiotParam.psswd,CONFIG_MESH_ROUTER_PASSWD);
    writeNVS();
    ESP_LOGI(NVFnodoTAG,"Parametros Fabrica guardados!");
    setDataDefault();
}



void readNVS(int inc) {
    // Initialize NVS				//Lo comento porque ya lo hago antes en el main.c para poner el AP y la p�gina web
	 esp_err_t err;
//    esp_err_t err = nvs_flash_init();
//    if (err == ESP_ERR_NVS_NO_FREE_PAGES)  { //En versiones mas nuevas  || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
//        // NVS partition was truncated and needs to be erased
//        // Retry nvs_flash_init
//        ESP_ERROR_CHECK(nvs_flash_erase());
//        err = nvs_flash_init();
//        setParamToDefault();
//    }
//    ESP_ERROR_CHECK( err );

    // Open
    nvs_handle my_handle;
    err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n");

        // Read
        printf("Reading restart counter from NVS ... \n");
        // setParamToDefault();

        // Read the size of memory space required for blob
        size_t required_size = 0;  // value will default to 0, if not set yet in NVS
        err = nvs_get_blob(my_handle, "nodoiotParam", NULL, &required_size);
        if (required_size==0) setParamToDefault();
        if (err == ESP_OK) {
        	if (required_size>0) {
        		if (required_size>sizeof(param)) required_size=sizeof(param);
        		err = nvs_get_blob(my_handle, "nodoiotParam", &nodoiotParam, &required_size);
        		if (err != ESP_OK) return;
        		if (inc>0) {
        			required_size=sizeof(param);
        			err = nvs_set_blob(my_handle, "nodoiotParam", &nodoiotParam, required_size);
        			err = nvs_commit(my_handle);
        		}
        	}
        } else {
            printf("Error (%s) reading!\n", esp_err_to_name(err));
        }
        required_size = 0;
        err = nvs_get_blob(my_handle, "nodoiotDatos", NULL, &required_size);
        if (required_size==0) setDataDefault();
        if (err == ESP_OK) {
        	if (required_size>0) {
        		if (required_size>sizeof(nodoiotDatos)) required_size=sizeof(nodoiotDatos);
        		err = nvs_get_blob(my_handle, "nodoiotDatos", &nodoiotDatos, &required_size);
        		if (err != ESP_OK) return;
        	}
        } else {
            printf("Error (%s) reading!\n", esp_err_to_name(err));
        }
        required_size = 0;
        err = nvs_get_blob(my_handle, "nodoiotMACs", NULL, &required_size);
        if (err == ESP_OK) {
        	if (required_size>0) {
        		if (required_size>sizeof(sw_macs)) required_size=sizeof(sw_macs);
        		err = nvs_get_blob(my_handle, "nodoiotMACs", &nodoiotMACs, &required_size);
        		if (err != ESP_OK) return;
        		if (inc>0) {
        			//setParametros(Arranque);
        			required_size=sizeof(sw_macs);
        			err = nvs_set_blob(my_handle, "nodoiotMACs", &nodoiotMACs, required_size);
        			err = nvs_commit(my_handle);
        		}
        	}
        } else {
            printf("Error (%s) reading!\n", esp_err_to_name(err));
        }

        // Close
        nvs_close(my_handle);
    }
    fflush(stdout);
    // esp_restart();
}

esp_err_t writeNVS(void) {
    nvs_handle my_handle;
    esp_err_t err;

    // Open
    err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;

    // Write
    size_t required_size = sizeof(param);
    err = nvs_set_blob(my_handle, "nodoiotParam", &nodoiotParam, required_size);
    if (err != ESP_OK) return err;

    // Commit
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;

    // Close
    nvs_close(my_handle);

    //if (err == ESP_OK) printf("Write ok\n");

    return ESP_OK;


}
*/
esp_err_t writeNVSdatos(void) {
    nvs_handle my_handle;
    esp_err_t err;

    // Open
    err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;

    // Write
    size_t required_size = sizeof(nodoiotDatos);
    err = nvs_set_blob(my_handle, "nodoiotDatos", &nodoiotDatos, required_size);
    if (err != ESP_OK) return err;

    // Commit
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;

    // Close
    nvs_close(my_handle);

    return ESP_OK;
}

esp_err_t writeNVSmacs(void) {
    nvs_handle my_handle;
    esp_err_t err;

    // Open
    err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;

    // Write
    size_t required_size = sizeof(sw_macs);
    err = nvs_set_blob(my_handle, "nodoiotMACs", &nodoiotMACs, required_size);
    if (err != ESP_OK) return err;

    // Commit
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;

    // Close
    nvs_close(my_handle);

    return ESP_OK;
}



void emparejar(uint8_t* mac) {


	int i=0;
	for(i=0;i<maxMAC;i++) {
	   if (memcmp(nodoiotMACs.mac[i].mac,mac,6)==0) {
		   ESP_LOGI("Enparejando", "Ya existe");
		   return;
	   }
	}
	uint8_t ceros[]={0,0,0,0,0,0};
	for(i=0;i<maxMAC;i++) {
		if (memcmp(nodoiotMACs.mac[i].mac,ceros,6)==0) {
			ESP_LOGI("Enparejando", "Nuevo! Emparejado %i",i);
			memcpy(nodoiotMACs.mac[i].mac,mac,6);
			if (i==maxMAC-1) i=0; // Vamos a porner a ceros la sigiente mac disponible
			else i++;
			memcpy(nodoiotMACs.mac[i].mac,ceros,6);
			writeNVSmacs();
			return;
		}
	}
	// Si ha llegado aquí, todas estabanm escritas y no había hueco
	ESP_LOGI("Enparejando", "Nuevo! Emparejado (sin hueco)");
	memcpy(nodoiotMACs.mac[0].mac,mac,6);
	memcpy(nodoiotMACs.mac[1].mac,ceros,6);
	writeNVSmacs();
}

bool emparejados(uint8_t* mac) {
	for(int i=0;i<maxMAC;i++)
	   if (memcmp(nodoiotMACs.mac[i].mac,mac,6)==0) {
		   ESP_LOGI("Enparejado", "Emparejado");
		   return true;
	   }
	ESP_LOGI("Enparejado", "NO NO NO Emparejado");
	return false;
}

void cambiapareja(uint8_t* macble,uint8_t* macrand) {
	for(int i=0;i<maxMAC;i++)
	   if (memcmp(nodoiotMACs.mac[i].mac,macble,6)==0) {
		   memcpy(nodoiotMACs.mac[i].mac,macrand,6);
		   writeNVSmacs();
		   ESP_LOGI("Enparejado", "Cambiada pareja");
		   return;
	   }
	ESP_LOGI("Enparejado", "NO cambio pareja");
}


void borraremparejados() {
    memset(&nodoiotMACs, 0,sizeof(sw_macs));
    writeNVSmacs();
}
