/* Mesh Internal Communication Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_mesh.h"
#include "esp_mesh_internal.h"
#include "nvs_flash.h"

#include "gestion_sniffer.h"

//#include "savedata/jsonconfig.h"
#include "cmd_sniffer.h"
#include "listamacs.h"
#include "pcap.h"
//#include "vers.h"
#include "nvf.h"
//#include "timer_group_example_main.c"

uint8_t MisMACS[32];
xQueueHandle timer_queue;	//Handle del timer

uint8_t miChannel=1;
static const char *MESH_TAG = "SNIFF";
float snr[20]; // Canales hasta el 11 pero empezamos en 1



void sniffer_main(void)
{

	espera = false;
    // Init el array de MACs
    initMAC();

    // SNIFFER
    ESP_LOGW(MESH_TAG,"INIT SNIFFER");
    sniffInit();
    ESP_LOGW(MESH_TAG,"RUN SNIFFER");
    do_sniffer_start(miChannel,snr);


    printf("\n-------------------------------------- \n");
    printf("	USUARIOS DETECTADOS: \n");
    printf("-------------------------------------- \n\n");


    vTaskDelay(10000 / portTICK_PERIOD_MS);

    	while(1){

    		//Checkea si del buffer de 500 macs posibles, queda alguna registrada o no. Si no la hay, no hay usuario cerca
    		actualizaUsuario();

    		vTaskDelay(1000 / portTICK_PERIOD_MS);

    	}
}

