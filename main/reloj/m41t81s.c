/*
 * m41t81s.c
 *
 *  Created on: 29 abr. 2021
 *      Author: ismael.casaban
 */

#include "m41t81s.h"

#include <string.h>
#include <esp_idf_lib_helpers.h>

#include <esp_log.h>

#define I2C_FREQ_HZ 1000000  // Max 1MHz for esp-idf

/*Register address definitions*/
#define rtc_pseconds_reg	0x00
#define rtc_seconds_reg		0x01
#define rtc_minutes_reg		0x02
#define rtc_hours_reg 		0x03
#define rtc_dow_reg 		0x04
#define rtc_date_reg 		0x05
#define rtc_month_reg 		0x06
#define rtc_year_reg		0x07

#define CHECK_ARG(VAL) do { if (!(VAL)) return ESP_ERR_INVALID_ARG; } while (0)

uint8_t bcdToDec(uint8_t bcd);
uint8_t decToBCD(uint8_t dec);
esp_err_t readRegister(m41t81s_t *dev, uint8_t reg, uint8_t *val);
esp_err_t writeRegister(m41t81s_t *dev, uint8_t reg, uint8_t *val);

esp_err_t readRegister_num(m41t81s_t *dev, uint8_t reg, uint8_t *val, uint8_t num);
esp_err_t writeRegister_num(m41t81s_t *dev, uint8_t reg, uint8_t *val, uint8_t num);

esp_err_t m41t81s_init(m41t81s_t *dev, i2c_port_t port, uint8_t addr, gpio_num_t sda_gpio, gpio_num_t scl_gpio){

	CHECK_ARG(dev);

	dev->port = port;
	dev->addr = addr;
	dev->cfg.sda_io_num = sda_gpio;
	dev->cfg.scl_io_num = scl_gpio;
#if HELPER_TARGET_IS_ESP32
	dev->cfg.master.clk_speed = I2C_FREQ_HZ;
#endif

	esp_err_t rst = i2c_dev_create_mutex(dev);

	if(rst != ESP_OK) return rst;

	uint8_t aux = 0x3F;
	return writeRegister(dev, 0x0C, &aux);

}

esp_err_t m41t81s_deinit(m41t81s_t *dev){
	CHECK_ARG(dev);

	return i2c_dev_delete_mutex(dev);
}

esp_err_t m41t81s_reset(m41t81s_t *dev){

	uint8_t aux[] = {0, 0, 0, 0};

	return writeRegister_num(dev, 0x00, aux, 4);

}

esp_err_t m41t81s_get_time(m41t81s_t *dev, struct tm * time){

	CHECK_ARG(dev && time);

	memset(time, 0, sizeof(struct tm));

	uint8_t aux[7];
	memset(aux, 0, 7);

	//Read All registers
	readRegister_num(dev, rtc_seconds_reg, aux, 16);

	time->tm_sec = bcdToDec(aux[0] & 0x7F);		// Seconds
	time->tm_min = bcdToDec(aux[1] & 0x7F);		// Minutes
	time->tm_hour = bcdToDec(aux[2] & 0x3F);	// Days
	time->tm_wday = bcdToDec(aux[3] & 0x07);	// Day of the week
	time->tm_mday = bcdToDec(aux[4] & 0x3F);	// Day of the year
	time->tm_mon = bcdToDec(aux[5] & 0x1F);		// Month
	time->tm_year = (bcdToDec(aux[6]));	// Year

	time->tm_isdst = -1;	//Not available

	return ESP_OK;

}


esp_err_t m41t81s_set_time(m41t81s_t *dev, struct tm * time){

	CHECK_ARG(dev && time);

	uint8_t aux[7];

	memset(aux, 0, 7);

	//Read Seconds
	aux[0] = (uint8_t)decToBCD(time->tm_sec);
	aux[1] = (uint8_t)decToBCD(time->tm_min);
	aux[2] = (uint8_t)decToBCD(time->tm_hour);
	aux[3] = (uint8_t)decToBCD(time->tm_wday);
	aux[4] = (uint8_t)decToBCD(time->tm_mday);
	aux[5] = (uint8_t)(decToBCD(time->tm_mon) & 0x1F);
	aux[6] = (uint8_t)(decToBCD(time->tm_year));

	return writeRegister_num(dev, rtc_seconds_reg, aux, 7);

}




uint8_t bcdToDec(uint8_t bcd) {
	return (uint8_t)(bcd & 0x0F) + ((bcd >> 4) * 10);
}

uint8_t decToBCD(uint8_t dec) {
	return (uint8_t)((dec / 10) << 4) + (dec % 10);
}

esp_err_t readRegister(m41t81s_t *dev, uint8_t reg, uint8_t *val) {

	return readRegister_num(dev, reg, val, 1);

}

esp_err_t writeRegister(m41t81s_t *dev, uint8_t reg, uint8_t *val){

	return writeRegister_num(dev, reg, val, 1);

}

esp_err_t readRegister_num(m41t81s_t *dev, uint8_t reg, uint8_t *val, uint8_t num){

	CHECK_ARG(dev && val);

	I2C_DEV_TAKE_MUTEX(dev);
	I2C_DEV_CHECK(dev, i2c_dev_read_reg(dev, reg, val, num));
	I2C_DEV_GIVE_MUTEX(dev);

	return ESP_OK;

}

esp_err_t writeRegister_num(m41t81s_t *dev, uint8_t reg, uint8_t *val, uint8_t num){

	CHECK_ARG(dev);

	I2C_DEV_TAKE_MUTEX(dev);
	I2C_DEV_CHECK(dev, i2c_dev_write_reg(dev, reg, val, num));
	I2C_DEV_GIVE_MUTEX(dev);

	return ESP_OK;

}
