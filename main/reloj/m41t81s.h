/*
 * m41t81s.h
 *
 *  Created on: 29 abr. 2021
 *      Author: ismael.casaban
 */

#ifndef COMPONENTS_SENSORS_INCLUDE_M41T81S_H_
#define COMPONENTS_SENSORS_INCLUDE_M41T81S_H_

#include <stddef.h>
#include <stdbool.h>
#include <i2cdev.h>
#include <driver/spi_master.h>
#include <driver/gpio.h>
#include <esp_err.h>

#include <time.h>

#define M41T81S_ADDR_BASE 0x68

typedef i2c_dev_t m41t81s_t;

esp_err_t m41t81s_init(m41t81s_t *dev, i2c_port_t port, uint8_t addr, gpio_num_t sda_gpio, gpio_num_t scl_gpio);

esp_err_t m41t81s_deinit(m41t81s_t *dev);

esp_err_t m41t81s_reset(m41t81s_t *dev);

esp_err_t m41t81s_get_time(m41t81s_t *dev, struct tm * time);
esp_err_t m41t81s_set_time(m41t81s_t *dev, struct tm * time);

#endif /* COMPONENTS_SENSORS_INCLUDE_M41T81S_H_ */
