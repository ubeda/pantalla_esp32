/*
 * display.h
 *
 *  Created on: 3 may. 2021
 *      Author: ismael.casaban
 */

#ifndef MAIN_DISPLAY_H_
#define MAIN_DISPLAY_H_

#include <stdint.h>
#include <stdbool.h>
#include "time.h"

#define MAX_LINEAS_PIXELS 		64
#define MAX_COLUMNAS_PIXELS 	128

#define MAX_LINEAS				8
#define MAX_COLUMNAS			21
#define PIXELS_CARACTER			6


// AJUSTE_COLUMNAS permite centrar en el display segun el numero de caracteres que se haya
// fijado y el ancho en pixels por caracter que en principio es 6 FIJO...
// AJUSTE_COLUMNAS = (MAX_COLUMNAS_PIXELS - (MAX_COLUMNAS * PIXELS_CARACTER)) / 2
#if MAX_COLUMNAS == 21
#define AJUSTE_COLUMNAS	1
#else
#define AJUSTE_COLUMNAS 4
#endif

#define ALTO_DIGITO		16
#define ANCHO_DIGITO	13

/*************************************************************************/

#define TECLA_ARRIBA 	4
#define TECLA_ABAJO 	8
#define TECLA_DERECHA 	7
#define TECLA_IZQUIERDA	5
#define TECLA_ACEPTAR 	6

#define TECLA_1			0
#define TECLA_2			1
#define TECLA_3			2
#define TECLA_4			3

/*************************************************************************/

/* ------------------------------------------------------
   Funcion: IniciarDisplay(void)
   Inicializacion del controlador de display...
   ------------------------------------------------------ */
void IniciarDisplay (void);
void ControlLCD (uint8_t valor, uint8_t modo);

/* ------------------------------------------------------
   Funcion: BorraPagina()
   ------------------------------------------------------ */
void BorraPagina(void);

/* ------------------------------------------------------
   Funcion: VolcarPagina(lCursor, cCursor)
   Vuelca en el display el contenido de LineasLCD,
   que contiene la informacion referida a la pagina activa
		*lCursor: 0..MAX_LINEAS-1, cCursor: 0..MAX_COLUMNAS-1
		*lCursor: 0xFF-No se muestra cursor
   ------------------------------------------------------ */
void VolcarPagina(uint8_t lCursor, uint8_t cCursor);

/* ------------------------------------------------------
   Funcion: PrintL(modoGrafico, linea, columna, centrar, cadena)
	 modoGrafico = 0: Modo Texto de altura 8 pixels (linea = 0..7, columna = 0..MAX_COLUMNAS-1)
	 modoGrafico = 1: Modo Texto de altura 16 pixels (linea = 0..7, columna = 0..MAX_COLUMNAS-1)
	 modoGrafico = 2: Modo Grafico de altura 8 pixels (linea = 0..63, columna = 0..127)
	 modoGrafico = 3: Modo Grafico de altura 16 pixels (linea = 0..63, columna = 0..127)

	 mofoGrafico: 	Mapa de bits con la configuración de la visualización en display
	 b0........Altura del caracter:	0- 8pixels, 1 -16pixels
	 b1........Modo grafico de visualización: 0- coordenadas en filas/columnas, 1- coordenadas en pixeles
	 b2........Modo opaco de visualización de texto 0- Se sobreescribe el texto sobre el fonfo 1- Se borra el fondo al escribir el texto
	 b3........Modo de blanco sobre negro...
   ------------------------------------------------------ */
uint8_t PrintL(uint8_t modoGrafico, uint8_t linea, uint8_t columna, uint8_t centrar, const char * cadena);

/* ------------------------------------------------------
   Funcion: VolcarImagen(uint8_t indice, uint8_t lineaPixel, uint8_t columnaPixel, uint8_t modo)
	 Si indice = 0..100, Seleccion de imagen
	 										 modo = 0: Modo Transparente
	 									 	 modo = 1: Modo Opaco
										   modo = 2: Modo Borrado del area del icono

	 Si indice = 0xFC, Caracteres de alto 16, modo indica el numero de caracteres
	                   Se visualizan en modo transparente
	 Si indice = 0xFD, Caracteres de alto 16, modo indica el numero de caracteres
	                   Se visualizan en modo opaco
	 Si indice = 0xFE, Caracteres de alto 8, modo indica el numero de caracteres
	                   Se visualizan en modo transparente
	 Si indice = 0xFF, Caracteres de alto 8, modo indica el numero de caracteres
	                   Se visualizan en modo transparente

	 Si indice = Indice de imagen...
	 					   modo = 0: Imagen transparente
							 modo = 1: Imagen opaca
							 modo = 2: Borrado de area de la imagen...
							 modo = 3: Para dibujar blanco sobre fondo negro...
   ------------------------------------------------------ */
void VolcarImagen(uint8_t indice, uint8_t lineaPixel, uint8_t columnaPixel, uint8_t modo);
void BorrarImagen(uint8_t lineaPixel, uint8_t columnaPixel, uint8_t filas, uint8_t columnas, uint8_t negro);

/***********************************************************************/

#define FLAG_16_PIXELS					0x01
#define FLAG_COORD_PIXELS				0x02
#define FLAG_OPACO						0x04
#define FLAG_BLANCO						0x08
#define FLAG_REDUCIDO       			0x10

#define AJUSTE_IZQUIERDA				'i'
#define AJUSTE_DERECHA					'd'
//#define AJUSTE_BINARIO				'b'
#define AJUSTE_HEXADECIMAL				'h'

/***********************************************************************/

#define CARACTER_GRADO					0xDF
#define CARACTER_CUADRO					0xFF
#define CARACTER_DERECHA				0x7E
#define CARACTER_IZQUIERDA				0x7F
#define CARACTER_NY_MAY					0xD1
#define CARACTER_NY_MIN					0xF1
#define CARACTER_MAS					0x00
#define CARACTER_MENOS					0x01
#define CARACTER_ACEPTAR				0x02
#define CARACTER_OMEGA					0xF4
#define CARACTER_ANTENA					0xB7
#define CARACTER_SEPARADOR				0xFE

/*************************************************************************/

#define I_FONDO							2
#define I_ALARMA						3
#define I_AVISO							4
#define I_CANDADO						8
#define I_PANTALLA_PRINCIPAL     		31
#define I_LUMINOSIDAD		     		32
#define I_MOTOR				     		33
#define I_MOTOR_DERECHA					34
#define I_MOTOR_IZQUIERDA				35
#define I_MOTOR_CIRCULO					36




#define I_ANTENA_OFF					10
#define I_ANTENA_1						11
#define I_ANTENA_2						12
#define I_ANTENA_3						13
#define I_GPS							14

#define I_DIGITO_0						20
#define I_DIGITO_1						21
#define I_DIGITO_2						22
#define I_DIGITO_3						23
#define I_DIGITO_4						24
#define I_DIGITO_5						25
#define I_DIGITO_6						26
#define I_DIGITO_7						27
#define I_DIGITO_8						28
#define I_DIGITO_9						29


/*************************************************************************/

#define MODEM_PWKEY_PIN    			23
#define MODEM_RESET_PIN    			22

#define DISPLAY_RES_PIN				27
#define DISPLAY_A0_PIN				2
#define DISPLAY_CCS_PIN				15
//#define DISPLAY_CLK_PIN				14
//#define DISPLAY_MOSI_PIN			13

#define DISPLAY_CLK_PIN				13
#define DISPLAY_MOSI_PIN			14

#define SDA_GPIO 					25
#define SCL_GPIO 					26
#define GPIO_EXP_IRQ_PIN			36

typedef struct {

	uint8_t Intermitencia;
	uint8_t Edicion;
	uint8_t Password[1];

	uint8_t Pagina;
	uint8_t PaginaPss;
	uint8_t ticksPagina;
	uint8_t Campo;

	uint8_t nTeclas;
	uint8_t Teclas[7];

	uint16_t Dato;

	uint8_t EstadoIO; // b0..b3: Led1..Led4, b4:Light display
	struct tm AuxReloj;

	uint16_t Programa;

} __attribute__ ((packed)) Interfaz_t;


#endif /* MAIN_DISPLAY_H_ */
