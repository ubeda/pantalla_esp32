#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "driver/uart.h"
#include "io.h"
#include "m41t81s.h"
#include "def.h"
#include "nvs.h"
#include "parse_json.h"
#include "stdint.h"
#include "stdbool.h"
#include "mqtt.h"
#include "nvs_flash.h"
#include "display.h"
#include "main.h"
#include "display.h"
#include "wifi_station.h"
#include "config.h"
#include "m41t81s.h"
#include "mqtt.h"

static const char *TAG = "MAIN";
uint32_t serverTime;   //Hora envio al servidor
m41t81s_t rtc;
struct tm mytime;
struct tm time_screen;			//Hora epoch con zona horaria
char *tz_screen;
char pass_interface[20];
Interfaz_t Interfaz;
esp_mqtt_client_config_t mqtt_cfg; //Create the MQTT configuration structure
timer_declaration(timer_refresh_display);
int versionFW = 100;
double valorLux;
double angulo = 0;
uint8_t error_motor = 0;
bool ask_lux = false;
bool ask_gyro = false;
float current_angle = 0;
short pitch;
short roll;

#define NOTE_C4  262
#define NOTE_D4  294
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_G4  392
#define NOTE_A4  440
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_GS4 415
#define NOTE_D4  294
#define NOTE_E4  330
#define NOTE_A4  440
#define NOTE_FS4 370
#define NOTE_C4  262
#define NOTE_D4  294
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_G4  392
#define NOTE_A4  440
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_AS4 466

int melody[] = { NOTE_C4, NOTE_C4, NOTE_D4, NOTE_C4, NOTE_F4, NOTE_E4, NOTE_C4, NOTE_C4, NOTE_D4, NOTE_C4, NOTE_G4, NOTE_F4, NOTE_C4, NOTE_C4, NOTE_C4, NOTE_A4, NOTE_F4, NOTE_E4, NOTE_D4, NOTE_G4, NOTE_G4, NOTE_F4, NOTE_E4, NOTE_D4, NOTE_C4 };
int noteDurations[] = { 63, 63, 125, 125, 125, 250, 63, 63, 125, 125, 125, 250, 63, 63, 125, 125, 125, 125, 250, 63, 63, 125, 125, 125, 250 };

volatile bool playing = true;  // Variable de control de reproducci�n
const float songSpeed = 1.0;
const int buzzer = 10;


// Funci�n para hacer sonar una nota en el buzzer
void playTone(int frequency, int duration) {
    int period = 1000000 / frequency;
    int halfPeriod = period / 2;
    int cycles = duration * frequency / 1000;

    for (int i = 0; i < cycles; i++) {
    	board_io_set_buzzer(1);
        ets_delay_us(halfPeriod);
    	board_io_set_buzzer(0);
        ets_delay_us(halfPeriod);
    }
}

//Configuracion de fabrica. Se llamara a esta funcion cuando se inicie por primera vez la particion NVS
int setParamToDefault(char* dataname, void* data, int datalenght){

	if (strcmp("misparametros",dataname)==0) {
		ESP_LOGW("Param","Parametros Fabrica guardados!");
	}
	return 0;
}



void app_main(void)
{
	ESP_LOGI(TAG, "Main start up.");

	initNVS(setParamToDefault);
    readNVS("misparametros",NULL, sizeof(int));

	board_io_init();

    //Iniciamos configuracion y conexion de wifi station
    wifi_init_sta();

    //Poner por defecto parametros display
	memset(&Interfaz,0,sizeof(Interfaz_t));
	Interfaz.Pagina = 0;
	Interfaz.ticksPagina = 0;
	Interfaz.Intermitencia = 0;
	Interfaz.EstadoIO = 0;

	IniciarDisplay();										//Iniciar display
	board_io_set_lcd_light(1);								//Enciende luz del display
	board_io_set_leds(Interfaz.EstadoIO | 0x10 | 0x20); 	//Apaga LEDs, mantiene display y modem encendidos

	//BorraPagina();

	//Primera pantalla
	//VolcarImagen(I_PANTALLA_PRINCIPAL, 0, 0, 0);
	//VolcarPagina(0xFF, 0xFF);

	//Manten la primera pantalla durante 4 ticks
	Interfaz.Pagina = 0;
	Interfaz.ticksPagina = 5 * 1;

	//Se especifica la zona horaria
	rtc_time_start_timezone("CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00");

	//Inicializa la comunicacion I2C del reloj
	m41t81s_init(&rtc, 0, M41T81S_ADDR_BASE, SDA_GPIO, SCL_GPIO);

	//Leo hora del reloj externo. El reloj externo no necesita el tz, siempre estara en utc
	m41t81s_get_time(&rtc, &mytime);

	rtc_time_print_tm("HORA DEL RELOJ: ", &mytime);		//Muestro hora del reloj

	//Le pongo al espressif la hora del reloj externo
	rtc_time_set_time_epoch_with_tz(mktime(&mytime),"CET-1CEST,M3.5.0,M10.5.0/3");	  //set con utc y tz

	//Crea una tarea para que gestione paralelamente las teclas de la placa
    xTaskCreate(board_io_loop, "board_io_loop", 8000, NULL, 50, NULL);

	ESP_ERROR_CHECK(esp_event_handler_register(EVENTS_KEYBOARD, ESP_EVENT_ANY_ID, &keyboard_event_handler, NULL));  //Gestion teclas

	timer_creation(timer_refresh_display, timer_refresh_display_cb);	//Timer refresco de display
	timer_start_periodic(timer_refresh_display, 500);

	char* cmnd = malloc(100*sizeof(char));
	char* msg = malloc(100*sizeof(char));

	memset(cmnd,0,100*sizeof(char));
	memset(msg,0,100*sizeof(char));

	board_io_set_buzzer(0);

	while(1){

		if(MQTTconectado){
			if(ask_lux){
				sendMQTT("station/7C87CEE34580/COMANDO","{\"cmd\":\"LUX\"}");	//Envia comando al nodo solicitando la luminosidad
			}
//			if(ask_gyro){
//				sendMQTT("station/7C87CEE34580/COMANDO","{\"cmd\":\"GYRO\"}");	//Envia comando al nodo solicitando la giroscopio
//			}
		}

		SLEEP_MS(5000);
	}

}

//Timer que salta cada 0.5 segundos para refrescar el display
void timer_refresh_display_cb(void* arg) {
	esp_event_post(EVENTS_KEYBOARD, EVENTS_TIMER_REFRESH_DISPLAY, NULL, 0, pdMS_TO_TICKS(200) );
}

/*
 * Pantalla esta suscrito al topic STATUS
 * Nodo esta suscrito al topic COMANDO
 *
 * LUMINOSIDAD
 * Pantalla envia topic: "station/7C87CEE34580/COMANDO" con mensaje: {"cmd":"LUX"}
 * Nodo responde topic: "station/C4DD57B81438/LUX" con mensaje: "{"cmd":"LUX","value":0.01}"
 * Pantalla recibe topic: "station/7C87CEE34580/LUX" con mensaje: "{"cmd":"LUX","value":0.01}"
 *
 * GIROSCOPIO
 * Pantalla envia topic: "station/7C87CEE34580/COMANDO" con mensaje: {"cmd":"GYRO"}
 * Nodo responde topic: "station/C4DD57B81438/LUX" con mensaje: "{"cmd":"GYRO","pitch":0.01,"roll":0.01}"
 * Pantalla recibe topic: "station/7C87CEE34580/LUX" con mensaje: "{"cmd":"GYRO","pitch":0.01,"roll":0.01}"
 *
 * MOTOR
 * Pantalla envia topic: "station/7C87CEE34580/COMANDO" con mensaje: {"cmd":"MOTOR","set":25}
 * Nodo responde topic: "station/C4DD57B81438/STATUS" con mensaje: "{"cmd":"MOTOR","set":0.01,"error":"NO"}"
 * Pantalla recibe topic: "station/7C87CEE34580/STATUS" con mensaje: "{"cmd":"MOTOR","set":0.01,"error":"NO"}"
 */

int callback_MQTT(char *data){   //Cada vez que recibamos un mensaje MQTT del exterior (mqtt explorer o web), saltar� esta funci�n.

	cJSON *root;

	root = checkJSON(data);			//Recogemos el JSON que hemos recibido en data
	if (root==NULL) ESP_LOGE("MAIN","MQTT: Root not created");

	char* cmd=NULL;
	char* command = malloc(100*sizeof(char));
	char* message = malloc(100*sizeof(char));
	double check_lux = 0;


	memset(command,0,100*sizeof(char));
	memset(message,0,100*sizeof(char));

    if ((cmd=getStringMyJSON(root,"cmd"))!=NULL) {	// {"cmd":"<comando>"}, donde <comando> puede ser INFO, STATUS, ORDER, etc
		   if (strcmp(cmd,"LUX")==0){							//Recibo comando INFO
			   check_lux = getNumberMyJSON(root,"value");
			   if(check_lux >= 0) valorLux = check_lux;
			   ESP_LOGW(TAG,"Recibimos comando LUX: %.2f",valorLux);
		   }

		   if (strcmp(cmd,"GYRO")==0){							//Recibo comando INFO
			   pitch = getNumberMyJSON(root,"pitch");
			   roll = getNumberMyJSON(root,"roll");
			   ESP_LOGW(TAG,"Recibimos comando GYRO: pitch: %d , roll: %d",pitch,roll);
		   }

		   if (strcmp(cmd,"MOTOR")==0){	//Si "cmd" es "MOTOR"
			   ESP_LOGI(TAG,"Recibimos comando motor");

			   float angle_set = getNumberMyJSON(root,"set");				//Nos guardamos el angulo real del motor
			   current_angle = getNumberMyJSON(root,"get");				//Nos guardamos el angulo real del motor
			   error_motor = getNumberMyJSON(root,"error");				//Nos guardamos el posible error ocurrido

			   //Si recibimos modo auto, actualizamos el valor del angulo para que cuando salga del modo auto, el angulo este actualizado
			   if(angle_set==365) angulo = current_angle;

			   if(!error_motor) ESP_LOGI(TAG,"No hay error en el motor");
			   else ESP_LOGE(TAG,"Hay error en el motor"); //Si hay error
		   }

    }

	cJSON_Delete(root);			//Borramos el paquete que hemos recibido

    return 0;

}

void CambiarPagina(uint8_t pagina, uint8_t auxiliar)
{
	Interfaz.Pagina = pagina;

	Interfaz.PaginaPss = auxiliar;
	Interfaz.Campo = 0;
	Interfaz.Edicion = 0;

	Interfaz.nTeclas = 0;
	Interfaz.Teclas[0] = '0';
	Interfaz.Teclas[1] = '0';
	Interfaz.Teclas[2] = '0';
	Interfaz.Teclas[3] = '0';
	Interfaz.Teclas[4] = '/0';
}

void keyboard_event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
	char txt[30];
	int numero, a, b, c, d;
	char* mess = malloc(100*sizeof(char));

	if((time(NULL) - timeScreen) > (10*60)){		//Si estamos mas de 10 minutos sin pulsar ninguna tecla...
		CambiarPagina(1,0);					//Volvemos a pantalla principal
		board_io_set_lcd_light(0);			//Apagamos lcd light
		timeScreen = 0;						//Reseteamos contador de tiempo
	}

	// Cada vez que pulse � suelte una tecla � expire el timer de 2 veces por segundo, salta este callback...
	switch (event_id) {
		case EVENTS_TIMER_REFRESH_DISPLAY:
			// Control de intermitencias...
			Interfaz.Intermitencia = !Interfaz.Intermitencia;
			// Control de retardo entre pantallas...
			if (Interfaz.ticksPagina) Interfaz.ticksPagina--;
			break;
		default:
			if (event_id >= EVENTS_KEYBOARD_KEY_1 && event_id <= EVENTS_KEYBOARD_NO_KEY_DOWN)
			break;
	}

	// Control de pulsaciones y temporizaciones de cambio de pantalla...
	switch (Interfaz.Pagina)
	{
		case 0: // Pantalla de inicio...
//			if (Interfaz.ticksPagina == 0) {
//				Interfaz.Pagina = 1; // Pantalla principal de estado del tren de lavado...
//			}
			if (Interfaz.Password[0]) CambiarPagina(3, 0);		//Si ya hemos introducido la password, cambiar a pantalla de menu
			else CambiarPagina(2, 10); 	//Si no, selecci�n de modelo con contrase�a...
			break;

		case 1: // Pantalla principal (luminosidad)
			switch(event_id) {
				case EVENTS_KEYBOARD_KEY_RIGHT:
					if (Interfaz.Password[0]) CambiarPagina(3, 0);		//Si ya hemos introducido la password, cambiar a pantalla de menu
					else CambiarPagina(2, 10); 	//Si no, selecci�n de modelo con contrase�a...
					break;
				case EVENTS_KEYBOARD_KEY_UP:
					CambiarPagina(7, 0); 	//Cambiar a pantalla de motor
					break;
				case EVENTS_KEYBOARD_KEY_DOWN:
					CambiarPagina(8, 0); 	//Cambiar a pantalla de giroscopio
					break;
				case EVENTS_KEYBOARD_KEY_LEFT:
					CambiarPagina(25, 0); 	//Cambiar a pantalla de Venturi Wall
					break;
			}
			break;

		case 25:
			switch(event_id) {
				case EVENTS_KEYBOARD_KEY_RIGHT:
					CambiarPagina(2, 10); 	//Cambiar a pantalla principal
					break;
				}
			break;
		case 7:	//Pantalla motor
			memset(mess,0,100*sizeof(char));
			switch(event_id) {
				case EVENTS_KEYBOARD_KEY_DOWN:
					CambiarPagina(1, 0); 	//Cambiar a pantalla de motor
					break;
				case EVENTS_KEYBOARD_KEY_LEFT:
					if(Interfaz.Campo != 5){	//Si no estamos en modo automatico, se puede controlar manualmente el motor
						if(current_angle>=10){
							Interfaz.Edicion = 1;
							Interfaz.Campo = 1;
							angulo = angulo - 10;
							snprintf(mess,100,"{\"cmd\":\"MOTOR\",\"set\":%.0f,\"get\":%d,\"error\":%d}",angulo,0,0);
							sendMQTT("station/7C87CEE34580/COMANDO",mess);
						}
					}
					break;
				case EVENTS_KEYBOARD_KEY_RIGHT:
					if(Interfaz.Campo != 5){	//Si no estamos en modo automatico, se puede controlar manualmente el motor
						if(current_angle<=290){
							Interfaz.Edicion = 1;
							Interfaz.Campo = 0;
							angulo = angulo + 10;
							snprintf(mess,100,"{\"cmd\":\"MOTOR\",\"set\":%.0f,\"get\":%d,\"error\":%d}",angulo,0,0);
							sendMQTT("station/7C87CEE34580/COMANDO",mess);
						}
					}
					break;
				case EVENTS_KEYBOARD_KEY_1:	//Ponemos el motor a minimo angulo
					if(Interfaz.Campo != 5){	//Si no estamos en modo automatico, se puede controlar manualmente el motor
						Interfaz.Edicion = 1;
						Interfaz.Campo = 2;
						angulo = 0;
						snprintf(mess,100,"{\"cmd\":\"MOTOR\",\"set\":%.2f,\"get\":%d,\"error\":%d}",angulo,0,0);
						sendMQTT("station/7C87CEE34580/COMANDO",mess);
					}
					break;
				case EVENTS_KEYBOARD_KEY_2:	//Ponemos el motor a maximo angulo
					if(Interfaz.Campo != 5){	//Si no estamos en modo automatico, se puede controlar manualmente el motor
						Interfaz.Edicion = 1;
						Interfaz.Campo = 3;
						angulo = 300;
						snprintf(mess,100,"{\"cmd\":\"MOTOR\",\"set\":%.2f,\"get\":%d,\"error\":%d}",angulo,0,0);
						sendMQTT("station/7C87CEE34580/COMANDO",mess);
					}
					break;
				case EVENTS_KEYBOARD_KEY_3:	//Ponemos el motor a mitad angulo
					if(Interfaz.Campo != 5){	//Si no estamos en modo automatico, se puede controlar manualmente el motor
						Interfaz.Edicion = 1;
						Interfaz.Campo = 4;
						angulo = 150;
						snprintf(mess,100,"{\"cmd\":\"MOTOR\",\"set\":%.2f,\"get\":%d,\"error\":%d}",angulo,0,0);
						sendMQTT("station/7C87CEE34580/COMANDO",mess);
					}
					break;
				case EVENTS_KEYBOARD_KEY_4:	//Activamos o desactivamos modo automatico
					if(Interfaz.Campo == 5) { //Si antes ya habiamos pulsado la tecla, estamos en modo auto
						Interfaz.Campo = 6;	//ponemos campo 6 para que deje de mostrar AUTO por pantalla
					} else Interfaz.Campo = 5;	//Si no habiamos pulsado la tecla, ponemos a campo 5 para que muestre AUTO
					Interfaz.Edicion = 1;
					snprintf(mess,100,"{\"cmd\":\"MOTOR\",\"set\":%.2f,\"get\":%d,\"error\":%d}",365.0,0,0);
					sendMQTT("station/7C87CEE34580/COMANDO",mess);
					break;
				case EVENTS_KEYBOARD_NO_KEY_RIGHT:
				case EVENTS_KEYBOARD_NO_KEY_LEFT:
				case EVENTS_KEYBOARD_NO_KEY_1:
				case EVENTS_KEYBOARD_NO_KEY_2:
				case EVENTS_KEYBOARD_NO_KEY_3:
				case EVENTS_KEYBOARD_NO_KEY_4:
					//Cuando soltamos la tecla, se dejara de mostrar la animacion pulsante de la interfaz
					Interfaz.Edicion = 0;
					break;

			}

			break;
		case 8: // Pantalla giroscopio
			switch(event_id) {
				case EVENTS_KEYBOARD_KEY_UP:
					CambiarPagina(1, 0); 	//Cambiar a pantalla de luminosidad
					break;
			}
			break;

		case 2: // Pantalla de comprobar password
				switch(event_id) {
					case EVENTS_KEYBOARD_KEY_UP:
						if (Interfaz.Teclas[Interfaz.nTeclas] < '9') Interfaz.Teclas[Interfaz.nTeclas]++;
						break;
					case EVENTS_KEYBOARD_KEY_DOWN:
						if (Interfaz.Teclas[Interfaz.nTeclas] > '0') Interfaz.Teclas[Interfaz.nTeclas]--;
						break;
					case EVENTS_KEYBOARD_KEY_LEFT:
						if (!Interfaz.nTeclas) CambiarPagina(1,0);		//Cambia a pantalla principal
						else Interfaz.nTeclas--;
						break;
					case EVENTS_KEYBOARD_KEY_RIGHT:
						if (Interfaz.nTeclas < 3) Interfaz.nTeclas++;
						break;
					case EVENTS_KEYBOARD_KEY_OK:

						 //Leemos password configurable
						 numero = atoi(pass_interface);				//Volcamos la contrase�a a la variable auxiliar "numero"
						 a = numero / 1000;												//Primer digito
						 b = (numero % 1000) / 100;										//Segundo digito
						 c = (numero % 1000) % 100 / 10;								//Tercer digito
						 d = ((numero % 1000) % 100) % 10 / 1;							//Cuarto digito
						 ESP_LOGW("MAIN","Se ha introducido %d%d%d%d, las password correctas son %d%d%d%d y 1217",Interfaz.Teclas[0]-48,Interfaz.Teclas[1]-48,Interfaz.Teclas[2]-48,Interfaz.Teclas[3]-48,a,b,c,d);

						if (((Interfaz.Teclas[0] == '1') && (Interfaz.Teclas[1] == '2') && (Interfaz.Teclas[2] == '3') && (Interfaz.Teclas[3] == '4')) ||
							((Interfaz.Teclas[0] == ('0'+a)) && (Interfaz.Teclas[1] == ('0'+b)) && (Interfaz.Teclas[2] == ('0'+c)) && (Interfaz.Teclas[3] == ('0'+d)))) {
							// Si la contrase�a es v�lida pasa a la p�gina indicada...
							Interfaz.Pagina = 3;
							Interfaz.Password[0] = 1;
							ESP_LOGW("MAIN","Password correcta");
						} else  {
							// Si la contrase�a no es v�lida, vuelve a la p�gina principal...
							Interfaz.Pagina = 1;
							Interfaz.Password[0] = 0;
							ESP_LOGE("MAIN","Password incorrecta");
						}
						break;
				}
				break;

		case 3: // Men�
			switch(event_id) {
				case EVENTS_KEYBOARD_KEY_UP:
					if (Interfaz.Campo) Interfaz.Campo--;
					break;
				case EVENTS_KEYBOARD_KEY_DOWN:
					if (Interfaz.Campo < 3) Interfaz.Campo++;
					break;
				case EVENTS_KEYBOARD_KEY_LEFT:
					Interfaz.Pagina = 1;
					break;
				case EVENTS_KEYBOARD_KEY_OK:
					switch (Interfaz.Campo) {
						case 0: CambiarPagina(4, 0); break;	//Cambiar a pantalla de editar fecha/hora
						case 1: CambiarPagina(5, 0); break;	//Cambiar a pantalla de editar password
						case 2: CambiarPagina(6, 0); break;	//Cambiar a pantalla de test
						case 3: CambiarPagina(6, 0); break;	//Cambiar a pantalla de test
					}
					break;
			}
			break;

		case 4: // Fecha / Hora
			switch(event_id) {
				case EVENTS_KEYBOARD_KEY_DOWN:
					if (Interfaz.Edicion) {
						if (Interfaz.Dato) Interfaz.Dato--;
					}
					break;
				case EVENTS_KEYBOARD_KEY_UP:
					if (Interfaz.Edicion) {
						switch(Interfaz.Campo){
							case 0: printf ("dia = %d \n",Interfaz.Dato); if(Interfaz.Dato < 31) {Interfaz.Dato++;} break;
							case 1: printf ("mes = %d \n",Interfaz.Dato); if(Interfaz.Dato < 11) {Interfaz.Dato++;} break;  //Menor de 11 porque el mes empieza en 0
							case 2: printf ("year = %d \n",Interfaz.Dato); Interfaz.Dato++; break;
							case 3: printf ("hora = %d \n",Interfaz.Dato); if(Interfaz.Dato < 23) {Interfaz.Dato++;} break;
							case 4: printf ("minuto = %d \n",Interfaz.Dato); if(Interfaz.Dato < 60) {Interfaz.Dato++;} break;
							case 5: printf ("segundo = %d \n",Interfaz.Dato); if(Interfaz.Dato < 60) {Interfaz.Dato++;} break;
						}
					}

					break;
				case EVENTS_KEYBOARD_KEY_LEFT:
					if (Interfaz.Campo == 0) CambiarPagina(3, 2);

					if (!Interfaz.Edicion) {
						if (Interfaz.Campo) Interfaz.Campo--;
					}

					break;

				case EVENTS_KEYBOARD_KEY_RIGHT:
					if (!Interfaz.Edicion) {
						if (Interfaz.Campo < 6) Interfaz.Campo++;
					}

					break;

				case EVENTS_KEYBOARD_KEY_OK:

				if (!Interfaz.Edicion)	{
					Interfaz.Edicion = 1;
					rtc_time_get_now_with_tz(&time_screen);
					Interfaz.AuxReloj = time_screen;
					switch (Interfaz.Campo) {
						case 0: Interfaz.Dato = Interfaz.AuxReloj.tm_mday; break;
						case 1: Interfaz.Dato = Interfaz.AuxReloj.tm_mon; break;
						case 2: Interfaz.Dato = Interfaz.AuxReloj.tm_year; break;
						case 3: Interfaz.Dato = Interfaz.AuxReloj.tm_hour; break;
						case 4: Interfaz.Dato = Interfaz.AuxReloj.tm_min; break;
						case 5: Interfaz.Dato = Interfaz.AuxReloj.tm_sec; break;
					}
				} else {
					Interfaz.Edicion = 0;
					switch (Interfaz.Campo) {
						case 0: Interfaz.AuxReloj.tm_mday = Interfaz.Dato; break;
						case 1: Interfaz.AuxReloj.tm_mon  = Interfaz.Dato; break;
						case 2: Interfaz.AuxReloj.tm_year = Interfaz.Dato; break;
						case 3: Interfaz.AuxReloj.tm_hour = Interfaz.Dato; break;
						case 4: Interfaz.AuxReloj.tm_min  = Interfaz.Dato; break;
						case 5: Interfaz.AuxReloj.tm_sec  = Interfaz.Dato; break;

						memset(Interfaz.Dato,0,sizeof(Interfaz.Dato));

					}

					//La hora que insertar�n por pantalla NO ser� GMT, sino que ser� la local al usuario.
					//A partir de esa hora local, obtenemos la hora GMT para insertarla en el reloj y en el espresif

					time_t t = mktime(&Interfaz.AuxReloj);		//Tranforma estructura tm a estructura time_t

					struct tm * tmp;

					tmp = gmtime(&t);		//Obtenemos la hora GMT a partir de la hora local

					//Guardamos en el reloj la hora GMT
					if (m41t81s_set_time(&rtc, tmp) == ESP_OK) ESP_LOGI("MAIN","Set time correctly");

					struct tm hora_guardada;

					//Obtenemos del reloj la hora que hemos guardado antes
					m41t81s_get_time(&rtc, &hora_guardada);

					//Mostramos la hora GMT guardada en el reloj
					rtc_time_print_tm("HORA GUARDADA EN EL RELOJ: ", &hora_guardada);

					//Al espressif le introduzco la hora GMT y la timezone
					rtc_time_set_time_with_tz(&hora_guardada,"CET-1CEST,M3.5.0,M10.5.0/3");

					//Imprimo la hora que he seteado al espressif, la cual ser� LOCAL porque le he introducido la timezone
					rtc_time_print("NUEVA HORA: ");

					struct tm new_time;

					//Obtengo la hora epoch local del espressif porque antes le he indicado la timezone
					rtc_time_get_now(&new_time);

					//Imprimo la hora epoch local del espressif
					rtc_time_print_tm("HORA A GUARDAR: ", &new_time);

					//Obtengo la hora que enviar� al servidor
					uint32_t serverTime = rtc_time_get_epochtime();

					}
			}

					break;


			case 5: // Pantalla de modificar password configurable
				switch(event_id) {
					case EVENTS_KEYBOARD_KEY_UP:
						if (Interfaz.Teclas[Interfaz.nTeclas] < '9') Interfaz.Teclas[Interfaz.nTeclas]++;
						break;
					case EVENTS_KEYBOARD_KEY_DOWN:
						if (Interfaz.Teclas[Interfaz.nTeclas] > '0') Interfaz.Teclas[Interfaz.nTeclas]--;
						break;
					case EVENTS_KEYBOARD_KEY_LEFT:
						if (!Interfaz.nTeclas) CambiarPagina(3,0);		//Cambia a menu
						else Interfaz.nTeclas--;
						break;
					case EVENTS_KEYBOARD_KEY_RIGHT:
						if (Interfaz.nTeclas < 3) Interfaz.nTeclas++;
						break;
					case EVENTS_KEYBOARD_KEY_OK:
						sprintf(pass_interface, "%d%d%d%d", Interfaz.Teclas[0]-48,Interfaz.Teclas[1]-48,Interfaz.Teclas[2]-48,Interfaz.Teclas[3]-48);
						ESP_LOGW("MAIN","Se ha introducido %d%d%d%d",Interfaz.Teclas[0]-48,Interfaz.Teclas[1]-48,Interfaz.Teclas[2]-48,Interfaz.Teclas[3]-48);
						ESP_LOGW("MAIN","Password modificada");
						writeNVS("misparametros",NULL,sizeof(int));		//Me guardo en la flash la contrase�a
						CambiarPagina(3, 0);		//Cambiar a pantalla de menu

						break;
				}
				break;

			case 6: // Pantalla de test
				switch(event_id) {
					case EVENTS_KEYBOARD_KEY_UP:
						break;
					case EVENTS_KEYBOARD_KEY_DOWN:
						playing = false;
						board_io_set_buzzer(0);
						break;
					case EVENTS_KEYBOARD_KEY_LEFT:
						CambiarPagina(3,0);		//Cambia a menu
						break;
					case EVENTS_KEYBOARD_KEY_RIGHT:
						playing = 1;
					    for (int i = 0; i < sizeof(melody) / sizeof(melody[0]); i++) {
					        playTone(melody[i], noteDurations[i]);
					        vTaskDelay(noteDurations[i] / portTICK_PERIOD_MS);
					    }

						break;
					case EVENTS_KEYBOARD_KEY_OK:
						break;
				}
				break;

	}

	// Refresco de la pantalla...
	BorraPagina();
	ask_lux = false;
	ask_gyro = false;
	switch (Interfaz.Pagina)
	{
		case 0: // Pantalla de inicio...
			//VolcarImagen(I_PANTALLA_PRINCIPAL, 0, 0, 0);

			strcpy(txt, "VENTURI");
			PrintL(FLAG_OPACO, 3, 4, 0, txt);

			strcpy(txt, "WALL");
			PrintL(FLAG_OPACO, 5, 5, 0, txt);

			break;

		case 1: // Pantalla luminosidad
			VolcarImagen(I_LUMINOSIDAD, 0, 0, 0);

			rtc_time_get_now_with_tz(&time_screen);

			// Fecha y hora arriba...
			strftime(txt, 20, "%H:%M:%S", &time_screen);
			PrintL(FLAG_COORD_PIXELS | FLAG_BLANCO, 1, 5, 0, txt);

			strftime(txt, 20, "%d/%m/%Y", &time_screen);
			PrintL(FLAG_COORD_PIXELS | FLAG_BLANCO, 1, 69, 0, txt);

			snprintf(txt, 20, "%.2f", valorLux);
			if(valorLux<10) PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 46, 15, 0, txt);
			else if(valorLux<100) PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 46, 13, 0, txt);
			else if(valorLux<1000) PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 46, 11, 0, txt);
			else PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 46, 10, 0, txt);


			// Marca de conexi�n con el servidor...
			if (MQTTconectado) {
				ask_lux = true;
				if (Interfaz.Intermitencia) VolcarImagen(I_ANTENA_1, 48, 115, 1);
				else VolcarImagen(I_ANTENA_3, 48, 115, 1);
			} else {
				VolcarImagen(I_ANTENA_OFF, 48, 115, 1);
			}

			break;

		case 25:
			strcpy(txt, "VENTURI");
			PrintL(FLAG_OPACO, 3, 7, 0, txt);

			strcpy(txt, "WALL");
			PrintL(FLAG_OPACO, 4, 8, 0, txt);

			if (Interfaz.Intermitencia) VolcarImagen(I_ANTENA_1, 48, 115, 1);
			else VolcarImagen(I_ANTENA_3, 48, 115, 1);

			break;
		case 7: // Pantalla motor
			VolcarImagen(I_MOTOR, 0, 0, 0);

			rtc_time_get_now_with_tz(&time_screen);

			if(Interfaz.Edicion) {
				switch(Interfaz.Campo){
					case 0:
						VolcarImagen(I_MOTOR_DERECHA, 23, 97, 1);
						break;
					case 1:
						VolcarImagen(I_MOTOR_IZQUIERDA, 23, 12, 1);
						break;
					case 2:
						VolcarImagen(I_MOTOR_CIRCULO, 47, 21, 1);
						break;
					case 3:
						VolcarImagen(I_MOTOR_CIRCULO, 47, 44, 1);
						break;
					case 4:
						VolcarImagen(I_MOTOR_CIRCULO, 47, 67, 1);
						break;
					case 5:
						VolcarImagen(I_MOTOR_CIRCULO, 47, 90, 1);
						break;
					default:
						break;
				}
			}

			// Fecha y hora arriba...
			strftime(txt, 20, "%H:%M:%S", &time_screen);
			PrintL(FLAG_COORD_PIXELS | FLAG_BLANCO, 1, 5, 0, txt);

			strftime(txt, 20, "%d/%m/%Y", &time_screen);
			PrintL(FLAG_COORD_PIXELS | FLAG_BLANCO, 1, 69, 0, txt);

			if(Interfaz.Campo==5){
				snprintf(txt, 20, "AUTO");
				PrintL(FLAG_OPACO | FLAG_COORD_PIXELS, 29, 54, 0, txt);
			} else {
				snprintf(txt, 20, "%.0f", current_angle);
				if(current_angle<10) PrintL(FLAG_OPACO | FLAG_COORD_PIXELS, 29, 62, 0, txt);
				else if(current_angle>=10 && current_angle<100) PrintL(FLAG_OPACO | FLAG_COORD_PIXELS, 29, 60, 0, txt);
				else if(current_angle>=100) PrintL(FLAG_OPACO | FLAG_COORD_PIXELS, 29, 57, 0, txt);
			}

			//Marca de conexi�n con el servidor
			if (MQTTconectado) {
				if (Interfaz.Intermitencia) VolcarImagen(I_ANTENA_1, 48, 115, 1);
				else VolcarImagen(I_ANTENA_3, 48, 115, 1);
			} else {
				VolcarImagen(I_ANTENA_OFF, 48, 115, 1);
			}

			if(error_motor){
				strcpy(txt, "Error");
				PrintL(FLAG_OPACO | FLAG_COORD_PIXELS, 10, 95, 0, txt);
			}

			break;

		case 8: // Pantalla giroscopio
			VolcarImagen(I_MOTOR, 0, 0, 0);

			rtc_time_get_now_with_tz(&time_screen);

			if(Interfaz.Edicion) {
				switch(Interfaz.Campo){
					case 0:
						VolcarImagen(I_MOTOR_DERECHA, 23, 97, 1);
						break;
					case 1:
						VolcarImagen(I_MOTOR_IZQUIERDA, 23, 12, 1);
						break;
					case 2:
						VolcarImagen(I_MOTOR_CIRCULO, 47, 21, 1);
						break;
					case 3:
						VolcarImagen(I_MOTOR_CIRCULO, 47, 44, 1);
						break;
					case 4:
						VolcarImagen(I_MOTOR_CIRCULO, 47, 67, 1);
						break;
					case 5:
						VolcarImagen(I_MOTOR_CIRCULO, 47, 90, 1);
						break;
					default:
						break;
				}
			}

			sendMQTT("station/7C87CEE34580/COMANDO","{\"cmd\":\"GYRO\"}");	//Envia comando al nodo solicitando la giroscopio

			if(roll>0) VolcarImagen(I_MOTOR_IZQUIERDA, 23, 12, 1);  //Mayor que 0, giramos a la izquierda
			else if(roll<0)	VolcarImagen(I_MOTOR_DERECHA, 23, 97, 1);	//Menor que 0, giramos a la derecha

			if(abs(pitch)>30) VolcarImagen(I_MOTOR_CIRCULO, 47, 21, 1);
			if(abs(pitch)>60) VolcarImagen(I_MOTOR_CIRCULO, 47, 44, 1);
			if(abs(pitch)>90) VolcarImagen(I_MOTOR_CIRCULO, 47, 67, 1);


			// Fecha y hora arriba...
			strftime(txt, 20, "%H:%M:%S", &time_screen);
			PrintL(FLAG_COORD_PIXELS | FLAG_BLANCO, 1, 5, 0, txt);
			strftime(txt, 20, "%d/%m/%Y", &time_screen);
			PrintL(FLAG_COORD_PIXELS | FLAG_BLANCO, 1, 69, 0, txt);

			//Muestra angulos pitch y roll
			snprintf(txt, 20, "%d", pitch);
			PrintL(FLAG_OPACO | FLAG_COORD_PIXELS, 29, 48, 0, txt);

			snprintf(txt, 20, "%d", roll);
			PrintL(FLAG_OPACO | FLAG_COORD_PIXELS, 29, 73, 0, txt);

			//Marca de conexi�n con el servidor
			if (MQTTconectado) {
				//ask_gyro = true;
				if (Interfaz.Intermitencia) VolcarImagen(I_ANTENA_1, 48, 115, 1);
				else VolcarImagen(I_ANTENA_3, 48, 115, 1);
			} else {
				VolcarImagen(I_ANTENA_OFF, 48, 115, 1);
			}

			break;

		case 2: //Pantalla de comprobar password
			BorrarImagen(0, 0, 9, 128, 1);
			strcpy(txt, "****** PASSWORD *****");
			PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 1, 0, 128, txt);
			VolcarImagen(I_CANDADO, 12, 36, 1);
			VolcarImagen(I_DIGITO_0 + (Interfaz.Teclas[Interfaz.nTeclas] - '0'), 40, 38 + (Interfaz.nTeclas * 13), 1);

			break;

		case 3: // Men�
			BorrarImagen(0, 0, 9, 128, 1);
			sprintf(txt, "%c                   %c", CARACTER_IZQUIERDA, CARACTER_DERECHA);
			PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 1, 0, 0, txt);

			sprintf(txt, "MENU");
			PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 1, 10, 108, txt);


			strcpy(txt, "Activar Motor");
			PrintL(FLAG_OPACO, 2, 1, 0, txt);
			strcpy(txt, "Leer Sensores");
			PrintL(FLAG_OPACO, 3, 1, 0, txt);
			strcpy(txt, "Autodestruir");
			PrintL(FLAG_OPACO, 4, 1, 0, txt);
			strcpy(txt, "Reproducir Canci�n");
			PrintL(FLAG_OPACO, 5, 1, 0, txt);

			snprintf(txt, 2, "%c", CARACTER_DERECHA); // Selecci�n...
			PrintL(FLAG_OPACO, 2 + Interfaz.Campo, 0, 0, txt);

			break;

		case 4: // Fecha / Hora
			BorrarImagen(0, 0, 9, 128, 1);
			sprintf(txt, "%c                   %c", CARACTER_IZQUIERDA, CARACTER_DERECHA);
			PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 1, 0, 0, txt);

			sprintf(txt, "FECHA / HORA");
			PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 1, 10, 108, txt);

			rtc_time_get_now_with_tz(&time_screen);

			Interfaz.AuxReloj = time_screen;

			// Fecha y hora arriba...
			strftime(txt, 20, "%d/%m/%Y %H:%M:%S", &Interfaz.AuxReloj);
			PrintL(FLAG_16_PIXELS, 4, 1, 0, txt);

			if(Interfaz.Edicion) {	//Si estoy editando..
				switch (Interfaz.Campo) {	//Dependiendo del camp
					case 0: snprintf(txt, 20, "%02d", Interfaz.Dato); PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 1, 0, txt); break;
					case 1: snprintf(txt, 20, "%02d", Interfaz.Dato+1);  PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 4, 0, txt); break;
					case 2: snprintf(txt, 20, "%02d", Interfaz.Dato+1900); PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 7, 0, txt); break;
					case 3: snprintf(txt, 20, "%02d", Interfaz.Dato); PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 12, 0, txt);  break;
					case 4: snprintf(txt, 20, "%02d", Interfaz.Dato);  PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 15, 0, txt);  break;
					case 5: snprintf(txt, 20, "%02d", Interfaz.Dato);  PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 18, 0, txt); break;
				}

			} else {

				rtc_time_get_now_with_tz(&time_screen);
				Interfaz.AuxReloj = time_screen;

				switch (Interfaz.Campo) {
					case 0: strftime(txt, 20, "%d", &Interfaz.AuxReloj); PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 1, 0, txt); break;
					case 1: strftime(txt, 20, "%m", &Interfaz.AuxReloj);  PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 4, 0, txt); break;
					case 2: strftime(txt, 20, "%Y", &Interfaz.AuxReloj); PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 7, 0, txt); break;
					case 3: strftime(txt, 20, "%H", &Interfaz.AuxReloj); PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 12, 0, txt);  break;
					case 4: strftime(txt, 20, "%M", &Interfaz.AuxReloj);  PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 15, 0, txt);  break;
					case 5: strftime(txt, 20, "%S", &Interfaz.AuxReloj);  PrintL(FLAG_16_PIXELS | FLAG_BLANCO, 4, 18, 0, txt); break;
				}
			}

			break;

		case 5: //Pantalla de modificar password configurable
			BorrarImagen(0, 0, 9, 128, 1);
			sprintf(txt, "%c                   %c", CARACTER_IZQUIERDA, CARACTER_DERECHA);
			PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 1, 0, 0, txt);
			sprintf(txt, "EDITAR CONTRASE�A");
			PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 1, 10, 108, txt);
			VolcarImagen(I_CANDADO, 12, 36, 1);
			if (Interfaz.Intermitencia) {
				VolcarImagen(I_DIGITO_0 + (Interfaz.Teclas[Interfaz.nTeclas] - '0'), 40, 38 + (Interfaz.nTeclas * 13), 1);
			} else {
				BorrarImagen(40, 38 + (Interfaz.nTeclas * 13), 16, 13, 0);
			}

			break;

		case 6: //Pantalla de test
//			BorrarImagen(0, 0, 9, 128, 1);
//			sprintf(txt, "%c                   %c", CARACTER_IZQUIERDA, CARACTER_DERECHA);
//			PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 1, 0, 0, txt);
//
//			sprintf(txt, "TEST");
//			PrintL(FLAG_BLANCO | FLAG_COORD_PIXELS, 1, 10, 108, txt);

			strcpy(txt, "Cumplea�os feliz");
			PrintL(FLAG_OPACO, 1, 2, 0, txt);

			strcpy(txt, "Cumplea�os feliz");
			PrintL(FLAG_OPACO, 3, 2, 0, txt);

			strcpy(txt, "Te deseamos todos");
			PrintL(FLAG_OPACO, 5, 2, 0, txt);

			strcpy(txt, "Cumplea�os feliz");
			PrintL(FLAG_OPACO, 7, 2, 0, txt);

			break;


	}
	VolcarPagina(0xFF, 0xFF);
}

