/*
 * config.h
 *
 *  Created on: 12 nov. 2021
 *      Author: PC
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include "esp_system.h"
#include "esp_heap_caps.h"
#include "esp_timer.h"
#include "esp_log.h"
#include "time.h"

extern uint8_t my_sntp_initialized;

#define SLEEP_MS(X)		vTaskDelay(pdMS_TO_TICKS(X))

#define AUX_CADENA(x) 	#x
#define CADENA(d) 		AUX_CADENA(d)

void rtc_time_get_now_with_tz(struct tm * time_now);
void rtc_time_get_now(struct tm * time_now);
void rtc_time_start_timezone(char * tz);
uint32_t rtc_time_get_epochtime(void);
uint32_t rtc_time_get_epochtime_tz(void);
void rtc_time_print_tm(char * txt, struct tm * time);
uint16_t rtc_time_get_ascii(char * buf, uint16_t buf_max);
void rtc_time_print(char * txt);
void rtc_time_set_time(struct tm * time_now);
void rtc_time_set_time_epoch(uint32_t time);
void rtc_time_set_time_epoch_with_tz(uint32_t time, char * tz);
void rtc_time_set_time_with_tz(struct tm * time_now, char * tz);

void timer_cb(void* arg);
void create_timer (esp_timer_handle_t *timer,int timer_period);
void start_periodic_timer(esp_timer_handle_t *timer,int timer_period);

void stop_timer(esp_timer_handle_t *timer);
void sntp_wrap_init(void);
void sntp_wrap_stop(void);


#define timer_declaration(NAME)										\
		esp_err_t err;												\
		static esp_timer_handle_t timer_##NAME;


#define timer_creation(NAME, FUNC)											\
		const esp_timer_create_args_t timer_args_##NAME = {					\
				.callback = &FUNC,											\
				.name = "timer_" CADENA(NAME)								\
};																			\
 	 	err = esp_timer_create(&timer_args_##NAME, &timer_##NAME);			\
 		if(err == ESP_OK) ESP_LOGI(TAG,"Timer created correctly");  		\
 		else ESP_LOGE(TAG,"Timer cannot be created correctly, err: %d",err);

#define timer_stop(NAME)																					\
		err = esp_timer_stop(timer_##NAME);																	\
		if(err == ESP_OK) ESP_LOGI(TAG,"Periodic timer stopped correctly");         						\
		else ESP_LOGW(TAG,"Periodic timer cannot be stopped correctly. err: %d",err);

#define timer_start_periodic(NAME, PERIOD_MS)																\
		timer_stop(NAME);																					\
		err = esp_timer_start_periodic(timer_##NAME, PERIOD_MS*1000);    									\
		if(err == ESP_OK) ESP_LOGI(TAG,"Periodic timer started correctly");         						\
		else ESP_LOGE(TAG,"Periodic timer cannot be started correctly. err: %d",err);

#define TIMER_START_ONCE(NAME, TIME_MS)								\
		timer_stop(NAME);											\
		esp_timer_start_once(timer_##NAME, TIME_MS*1000);

#define BIT_SET(p,n,t)  	((p) |= (t)(1 << (n)))
#define BIT_CLR(p,n,t)  	((p) &= (t)~((1) << (n)))
#define BIT_IS_SET(p,n,t) 	((p) & (t)((1) << (n)))
#define BIT_TOGGLE(p,n,t) 	((p) ^= (t)((1) << (n)))

#endif /* CONFIG_H_ */
