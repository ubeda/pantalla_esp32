/*
 * main.h
 *
 *  Created on: 24 may. 2021
 *      Author: mateo.ubeda
 */

#ifndef MAIN_MAIN_H_
#define MAIN_MAIN_H_

extern time_t startTime;
extern uint32_t horaActualEpoch;
extern void modem_power_on(void);

int siEntraDataBLE(char *data);
//int siConectaMQTT();
void app_main(void);
void uart_2_test_thread_task( void * pvParameters );
int siEntraDataMQTT(char *data);
void network_event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
void keyboard_event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
void timer_refresh_display_cb(void* arg);


#endif /* MAIN_MAIN_H_ */
