/*
 * def.h
 *
 *  Created on: 11 nov. 2020
 *      Author: ismael.casaban
 */

#ifndef MAIN_DEF_H_
#define MAIN_DEF_H_

#include <stdint.h>
#include <string.h>

#include "esp_system.h"
#include "config.h"

//#include "hal_can.h"
//#include "hal_rtc.h"

//#include "comm_common.h"

/**
 * PROJECT VERSION
 */
#define MAIN_PROJECT_VERSION		100


/**
 * Project definition
 */
#define MAIN_DEVICE_IDENTIFIER_STR_MAX_LEN	30

#define SLEEP_MS(X)		vTaskDelay(pdMS_TO_TICKS(X))

inline static void main_def_get_identification_mac(uint8_t mac[6]){

	memset(mac, 0, 6);

	esp_efuse_mac_get_default(mac); // BASE MAC
	mac[5] = mac[5] + 2; //Get BLE MAC

}

inline static void main_def_set_hostmane(char * t, uint16_t max_len){

	uint8_t mac[6];
	main_def_get_identification_mac(mac); // BASE MAC
	mac[5] = mac[5] + 2; //Get BLE MAC

	snprintf(t, max_len, CONFIG_DISARP_MAIN_PROJECT_NAME "_%02X%02X", mac[4],mac[5]);

}

inline static void main_def_set_mqtt_id(char * t, uint16_t max_len){

	uint8_t mac[6];
	main_def_get_identification_mac(mac); // BASE MAC

	snprintf(t, max_len, CONFIG_DISARP_MAIN_PROJECT_NAME "_%02X%02X%02X%02X%02X%02X", mac[0],mac[1],mac[2],mac[3], mac[4],mac[5]);


}

inline static void main_def_get_mac_ble(uint8_t mac[6]){

	memset(mac, 0, 6);

	esp_efuse_mac_get_default(mac); // BASE MAC
	mac[5] = mac[5] + 2; //Get BLE MAC

}

inline static void main_def_get_mac_wifi_station(uint8_t mac[6]){

	memset(mac, 0, 6);

	esp_efuse_mac_get_default(mac); // BASE MAC and WIFI MAC

}

inline static void main_def_get_mac_ethernet(uint8_t mac[6]){

	memset(mac, 0, 6);

	esp_efuse_mac_get_default(mac); // BASE MAC
	mac[5] = mac[5] + 3; //Get WETH MAC

}

inline static uint8_t main_def_get_identification_txt_from_mac(char * t, uint8_t mac[6], uint8_t use_dots){

	uint8_t rst_len = 0;

	if(use_dots){
		rst_len = snprintf(t, MAIN_DEVICE_IDENTIFIER_STR_MAX_LEN, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4],mac[5]);
	}else{
		rst_len = snprintf(t, MAIN_DEVICE_IDENTIFIER_STR_MAX_LEN, "%02X%02X%02X%02X%02X%02X", mac[0], mac[1], mac[2], mac[3], mac[4],mac[5]);
	}
	t[rst_len] = '\0';

	return rst_len;
}

inline static uint8_t main_def_get_identification_txt(char * t, uint8_t use_dots){

	uint8_t mac[6];
	memset(mac, 0, 6);

	esp_efuse_mac_get_default(mac); // BASE MAC
	//mac[5] = mac[5] + 2; //Get BLE MAC

	return main_def_get_identification_txt_from_mac(t, mac, use_dots);

}

inline static uint32_t main_def_get_epochtime(void){	//Obtiene el tiempo de calendario actual en formato time_t
	return rtc_time_get_epochtime();
}

inline static uint32_t main_def_get_epochtime_tz(void){
	return rtc_time_get_epochtime_tz();
}

/**
 * In Javascript, value if calculated with: (new Date().getTimezoneOffset() / 60).
 * In Spain (23/11/2020) this function returns -1 but we are in GMT+1 and UNIX needs <+01>-1
 */
inline static void main_def_timezone_convert_to_tz(int offset, char **user_readable, char **unix_tz){

	switch(offset){

	case -14: *user_readable = "GMT-14"; *unix_tz = "<-14>14"; break;
	case -13: *user_readable = "GMT-13"; *unix_tz = "<-13>13"; break;
	case -12: *user_readable = "GMT-12"; *unix_tz = "<-12>12"; break;
	case -11: *user_readable = "GMT-11"; *unix_tz = "<-11>11"; break;
	case -10: *user_readable = "GMT-10"; *unix_tz = "<-10>10"; break;
	case -9: *user_readable = "GMT-9"; *unix_tz = "<-9>9"; break;
	case -8: *user_readable = "GMT-8"; *unix_tz = "<-8>8"; break;
	case -7: *user_readable = "GMT-7"; *unix_tz = "<-7>7"; break;
	case -6: *user_readable = "GMT-6"; *unix_tz = "<-6>6"; break;
	case -5: *user_readable = "GMT-5"; *unix_tz = "<-5>5"; break;
	case -4: *user_readable = "GMT-4"; *unix_tz = "<-4>4"; break;
	case -3: *user_readable = "GMT-3"; *unix_tz = "<-3>3"; break;
	case -2: *user_readable = "GMT-2"; *unix_tz = "<-2>2"; break;
	case -1: *user_readable = "GMT-1"; *unix_tz = "<-1>1"; break;
	case 0: *user_readable = "GMT"; *unix_tz = "GMT0"; break;
	case 1: *user_readable = "GMT+1"; *unix_tz = "<+1>-1"; break;
	case 2: *user_readable = "GMT+2"; *unix_tz = "<+2>-2"; break;
	case 3: *user_readable = "GMT+3"; *unix_tz = "<+3>-3"; break;
	case 4: *user_readable = "GMT+4"; *unix_tz = "<+4>-4"; break;
	case 5: *user_readable = "GMT+5"; *unix_tz = "<+5>-5"; break;
	case 6: *user_readable = "GMT+6"; *unix_tz = "<+6>-6"; break;
	case 7: *user_readable = "GMT+7"; *unix_tz = "<+7>-7"; break;
	case 8: *user_readable = "GMT+8"; *unix_tz = "<+8>-8"; break;
	case 9: *user_readable = "GMT+9"; *unix_tz = "<+9>-9"; break;
	case 10: *user_readable = "GMT+10"; *unix_tz = "<+10>-10"; break;
	case 11: *user_readable = "GMT+11"; *unix_tz = "<+11>-11"; break;
	case 12: *user_readable = "GMT+12"; *unix_tz = "<+12>-12"; break;
	case 13: *user_readable = "GMT+13"; *unix_tz = "<+13>-13"; break;
	case 14: *user_readable = "GMT+14"; *unix_tz = "<+14>-14"; break;

	default:
		*user_readable = NULL;
		*unix_tz = NULL;
	}
}



#endif /* MAIN_DEF_H_ */
