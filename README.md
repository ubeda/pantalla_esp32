Interface Project
====================

This project is based on a ESP32 embedded in a PCB designed by an external company. The main feature of this board is the display and multiple buttons that it has, enabling a user-friendly board. Therefore, from now on, this board is called **Interface**, as it acts as the graphic interface for the users. Moreover, this project is tightly linked with another ESP32 project, called **Gateway**. This name comes from the fact that the user can perform multiple tasks by pressing the buttons and accessing to different menus on the **Interface** display and then, this board will command these petitions to the **Gateway**, which has different sensors and actuators connected.

The project incorporates WiFi technology, and after connecting to the network, it  sends and receives data to and from other devices through MQTT communication with messages in JSON format.

<br>

<img src=front.jpg alt="Ejemplo de imagen" width="600" height="450">

<br>
<br>
<br>


The system has different menus, which can be accessed through the buttons. Next, some menus are shown.

<br>

## Luminosity Menu

In the "Luminosity" menu, the user can get to know the current luminosity. The **Interface** automatically sends a command to **Gateway** via a MQTT message to ask for the current luminosity, and then, **Gateway** replies with the corresponding value. 

<br>

<img src=luminosity.gif alt="Descripción del GIF" autoplay>

<br>
<br>
<br>

## Motor Control Menu

The user can manually set the desired angle of the motor by pressing the bottons of the PCB of the **Interface** project. Then, this board sends a command via MQTT to **Gateway**, which receives it and sends the command to the servomotors. Moreover, while the angles change, the **Gateway** reads the current angle of the motor and sends it via MQTT to the **Interface** so it can display the angle value in the screen.

<br>

<img src=spin_on_button.gif alt="Descripción del GIF" autoplay>

<br>
<br>
<br>
